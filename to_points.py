"""This script reads the Robinsoncup file and creates the "equal" output with
points and ranks filled in.
"""
import argparse
import logging

from robinsoncup import argparse_functions

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)


def main(args):
    argparse_functions.to_points(args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input file, edited by user", type=str)
    parser.add_argument(
        "-o", "--output", help="Output filename", type=str, default="points.csv"
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="log level debug (else: info)"
    )
    args = parser.parse_args()

    main(args)
