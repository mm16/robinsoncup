"""This script reads the Robinsoncup file and creates the results (pdf,
certificates).
"""
import argparse
import logging

from robinsoncup import argparse_functions

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)


def main(args):
    argparse_functions.to_results(args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input",
        help="Input file, including points/ranks",
        type=str,
        default="points.csv",
    )
    parser.add_argument(
        "-o", "--output", help="Output filename", type=str, default="result.pdf"
    )
    parser.add_argument(
        "-c",
        "--certificates_output",
        help="Output filename for certificates",
        type=str,
        default="certificates.pdf",
    )
    parser.add_argument(
        "-n", "--no_open", help="Don't open created files", action="store_true"
    )
    parser.add_argument(
        "-t",
        "--text_only_certificates",
        help="Print certificates without logos",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--skip_certificates",
        help="Skip creating certificates",
        action="store_true",
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="log level debug (else: info)"
    )
    args = parser.parse_args()

    main(args)
