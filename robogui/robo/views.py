import logging
import os.path
from collections import namedtuple
from datetime import date

import numpy as np
from django.forms import modelformset_factory
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from robinsoncup import argparse_functions
from robinsoncup.teams import WERTUNGEN
from . import models
from . import csv_output

LOGGER = logging.getLogger(__name__)

NUMBER_OF_DISCIPLINES = 5
MAX_ATHLETES_PER_TEAM = 5

OUT_FILES = {
    "csv": "../0_input_auto-generated-from-robogui.csv",
    "startlist": "../1_startlist_auto-generated-from-robogui.pdf",
    "points": "../2_points_auto-generated-from-robogui.csv",
    "results": "../3_results_auto-generated-from-robogui.pdf",
    "certificates": "../4_certificates_auto-generated-from-robogui.pdf",
}

TEXT_ONLY_CERTIFICATES = True


def index(request):
    return render(request, "robo/index.html", {})


def csv_view(request, outputtype):
    allowed_outputtypes = ("csv", "startlist", "results")
    if outputtype not in allowed_outputtypes:
        return render(
            request,
            "robo/index.html",
            {
                "status_message": f"Request str not allowed. Allowed: {allowed_outputtypes}",
            },
        )

    status_message = None
    if outputtype == "csv":
        try:
            csv_output.data_to_csv(OUT_FILES["csv"])
            ArgParseArgs = namedtuple("ArgParseArgs", ["input", "output", "verbose"])
            to_points_args = ArgParseArgs(OUT_FILES["csv"], OUT_FILES["points"], False)
            argparse_functions.to_points(to_points_args)
        except Exception as e:
            status_message = f"Error: {str(e)}"
        else:
            LOGGER.info(f"Output written to {OUT_FILES['csv']}")
            LOGGER.info(f"Output written to {OUT_FILES['points']}")
            status_message = (
                f"2 files created: {OUT_FILES['csv']}, {OUT_FILES['points']}"
            )
    elif outputtype == "startlist":
        if not os.path.exists(OUT_FILES["csv"]):
            status_message = (
                f"Input file '{OUT_FILES['csv']}' does not exist -> create CSV first"
            )
        else:
            ArgParseArgs = namedtuple(
                "ArgParseArgs", ["input", "output", "no_open", "verbose"]
            )
            response = HttpResponse(
                content_type="application/pdf",
                headers={"Content-Disposition": 'attachment; filename="startlist.pdf"'},
            )
            try:
                to_startlist_args = ArgParseArgs(
                    OUT_FILES["csv"], OUT_FILES["startlist"], True, False
                )
                to_startlist_args_response = ArgParseArgs(
                    OUT_FILES["csv"], response, True, False
                )
            except Exception as e:
                status_message = f"Error: {str(e)}"
            else:
                argparse_functions.to_startlist(to_startlist_args)
                argparse_functions.to_startlist(to_startlist_args_response)
                return response
    elif outputtype == "results":
        if not os.path.exists(OUT_FILES["points"]):
            status_message = (
                f"Input file '{OUT_FILES['points']}' does not exist -> create csv first"
            )
        else:
            ArgParseArgs = namedtuple(
                "ArgParseArgs",
                [
                    "input",
                    "output",
                    "certificates_output",
                    "no_open",
                    "skip_certificates",
                    "verbose",
                    "text_only_certificates",
                ],
            )
            try:
                to_results_args = ArgParseArgs(
                    OUT_FILES["points"],
                    OUT_FILES["results"],
                    OUT_FILES["certificates"],
                    True,
                    False,
                    False,
                    TEXT_ONLY_CERTIFICATES,
                )
                argparse_functions.to_results(to_results_args)
            except Exception as e:
                status_message = f"Error: {str(e)}"
            else:
                status_message = f"Results, certificates created: {OUT_FILES['results']}, {OUT_FILES['certificates']}"

    return render(
        request,
        "robo/index.html",
        {
            "status_message": status_message,
        },
    )


def get_teams_of_wertung(teams, wertung: str) -> list:
    cur_year = date.today().year
    age_max, age_min = WERTUNGEN[wertung]
    teams_cur_wertung = []
    for t in teams:
        athletes_cur_team = models.ModelAthlete.objects.filter(team__id=t.id)
        mean_yob = np.mean([a.yob for a in athletes_cur_team])
        if (cur_year - age_max) <= mean_yob <= (cur_year - age_min):
            teams_cur_wertung.append(t)
    return teams_cur_wertung


class AthletesListView(generic.ListView):
    model = models.ModelAthlete
    context_object_name = "athletes"


class AthletesListPerClubView(AthletesListView):
    template_name = "robo/modelathleteperclub_list.html"

    def get_queryset(self):
        team_id = self.kwargs.get("pk")
        return models.ModelAthlete.objects.filter(team__id=team_id)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            team_name = models.ModelTeam.objects.get(id=self.kwargs["pk"]).name
        except models.ModelTeam.DoesNotExist:
            team_name = ""
        context.update({"team_name": team_name})
        return context


class AthleteCreateView(generic.CreateView):
    model = models.ModelAthlete
    fields = ["name", "yob"]
    template_name = "robo/create_form.html"

    def form_valid(self, form):
        team_id = self.kwargs.get("pk")
        form.instance.team = models.ModelTeam.objects.get(id=team_id)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("athletes-list", kwargs={"pk": self.kwargs["pk"]})


class AthleteUpdateView(generic.UpdateView):
    model = models.ModelAthlete
    fields = ["name", "yob", "team"]
    template_name = "robo/update_form.html"


class AthleteDeleteView(generic.DeleteView):
    model = models.ModelAthlete
    success_url = reverse_lazy("athletes-list")
    template_name = "robo/confirm_delete.html"


class TeamsView(generic.ListView):
    def get(self, request, *args, **kwargs):
        wertung = kwargs.get("wertung")
        if wertung is not None and wertung not in WERTUNGEN:
            raise Http404(f"Wertung {wertung} does not exist")
        teams = models.ModelTeam.objects.order_by("pk")
        number_teams_per_wertung = {
            w: len(get_teams_of_wertung(teams, w)) for w in reversed(WERTUNGEN.keys())
        }
        if wertung:
            teams_cur_wertung = get_teams_of_wertung(teams, wertung)
        else:
            teams_cur_wertung = list(teams)
        context = {
            "teams": teams_cur_wertung,
            "wertungen": number_teams_per_wertung,
            "n_total_teams": len(teams),
        }
        return render(
            request,
            "robo/teams.html",
            context,
        )


class ResultsPerDisciplineView(generic.UpdateView):
    def get(self, request, *args, **kwargs):
        discipline_id = kwargs.get("pk")
        discipline = models.ModelDiscipline.objects.get(pk=discipline_id)

        teams = models.ModelTeam.objects.order_by("pk")
        if kwargs.get("wertung"):
            teams = get_teams_of_wertung(teams, kwargs.get("wertung"))
        for t in teams:
            models.ModelResult.objects.get_or_create(
                team=t, discipline=discipline, defaults={"result": ""}
            )

        results_formset = modelformset_factory(
            models.ModelResult, fields=["team", "result"], max_num=len(teams)
        )
        results = models.ModelResult.objects.filter(
            discipline=discipline, team_id__in=[t.id for t in teams]
        ).order_by("team__id")
        results_form = results_formset(queryset=results)
        for form in results_form.forms:
            form.fields["team"].disabled = True
        return render(
            request,
            "robo/resultsperdiscipline.html",
            {
                "number_of_teams": len(teams),
                "results_form": results_form,
                "discipline": discipline,
                "wertungen": list(reversed(WERTUNGEN.keys())),
            },
        )

    def post(self, request, *args, **kwargs):
        discipline = models.ModelDiscipline.objects.get(pk=kwargs.get("pk"))
        teams = models.ModelTeam.objects.order_by("pk")
        if kwargs.get("wertung"):
            teams = get_teams_of_wertung(teams, kwargs.get("wertung"))
        results_formset = modelformset_factory(models.ModelResult, fields=["result"])
        results = models.ModelResult.objects.filter(
            discipline=discipline, team_id__in=[t.id for t in teams]
        ).order_by("team__id")
        results_form = results_formset(request.POST, queryset=results)
        n_results_updated = 0
        if results_form.is_valid():
            for res in results_form.cleaned_data:
                if res["id"] is None:
                    # this happens if no get request was sent before (only post request eg via unittests)
                    # current workaround: there must be results (objects) available and res["id"] must be that object,
                    # this means that form-?-id must be the id of the respective results object
                    raise NotImplementedError
                assert isinstance(res["id"], models.ModelResult)
                if res["id"].result != res["result"]:
                    res["id"].result = res["result"]
                    res["id"].save()
                    n_results_updated += 1
            status_message = f"{n_results_updated} result{'s' if n_results_updated>0 else ''} updated"
            return render(
                request,
                "robo/index.html",
                {
                    "status_message": status_message,
                },
            )

        context = {
            "error_message": results_form.errors,
            "number_of_teams": len(results_form),
            "discipline": discipline,
            "results_form": results_form,
            "wertungen": list(reversed(WERTUNGEN.keys())),
        }
        return render(request, "robo/resultsperdiscipline.html", context)


class TeamDeleteView(generic.DeleteView):
    model = models.ModelTeam
    success_url = reverse_lazy("teams")
    template_name = "robo/confirm_delete.html"


class TeamUpdateView(generic.UpdateView):
    model = models.ModelTeam
    fields = ["name", "verein", "startnumber"]
    template_name = "robo/update_form.html"


class TeamCreateView(generic.CreateView):
    model = models.ModelTeam
    fields = ["name", "verein", "startnumber"]
    template_name = "robo/create_form.html"
    success_url = reverse_lazy("teams")


class CompetitionListView(generic.ListView):
    model = models.ModelCompetition
    context_object_name = "competition"


class CompetitionCreateView(generic.CreateView):
    model = models.ModelCompetition
    fields = ["name", "date", "location", "points_per"]
    template_name = "robo/create_form.html"
    success_url = reverse_lazy("competition-list")


class CompetitionUpdateView(generic.UpdateView):
    model = models.ModelCompetition
    fields = ["name", "date", "location", "points_per"]
    template_name = "robo/update_form.html"


class CompetitionDeleteView(generic.DeleteView):
    model = models.ModelCompetition
    success_url = reverse_lazy("competition-list")
    template_name = "robo/confirm_delete.html"


class DisciplineListView(generic.ListView):
    model = models.ModelDiscipline
    context_object_name = "disciplines"


class DisciplineDetailView(generic.DetailView):
    model = models.ModelDiscipline


class DisciplineUpdateView(generic.UpdateView):
    model = models.ModelDiscipline
    fields = ["name", "smaller_is_better"]
    template_name = "robo/update_form.html"


class DisciplineCreateView(generic.CreateView):
    model = models.ModelDiscipline
    fields = ["name", "smaller_is_better"]
    template_name = "robo/create_form.html"


class DisciplineDeleteView(generic.DeleteView):
    model = models.ModelDiscipline
    success_url = reverse_lazy("disciplines-list")
    template_name = "robo/confirm_delete.html"
