from django.contrib import admin

from .models import ModelAthlete
from .models import ModelCompetition
from .models import ModelDiscipline
from .models import ModelTeam
from .models import ModelResult

class AthleteAdmin(admin.ModelAdmin):
    list_display = ("name", "yob", "team")
    list_filter = ["yob", "team__name"]


class TeamAdmin(admin.ModelAdmin):
    list_filter = ["verein"]


admin.site.register(ModelAthlete, AthleteAdmin)
admin.site.register(ModelCompetition)
admin.site.register(ModelDiscipline)
admin.site.register(ModelTeam, TeamAdmin)
admin.site.register(ModelResult)
