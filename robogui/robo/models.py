import datetime
import re

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.fields import validators
from django.urls import reverse

from robinsoncup import teams
from robinsoncup.io_base import REGEX_TIME


class ModelCompetition(models.Model):
    name = models.CharField(max_length=300)
    date = models.DateField(help_text="2022-12-24")
    location = models.CharField(max_length=200)
    points_per = models.IntegerField(
        choices=[(p.value, p.name) for p in teams.PointsPer],
        default=teams.PointsPer.Altersklasse,
    )

    def __str__(self):
        return f"{self.name}, {self.date} ({self.location})"

    def get_absolute_url(self):
        return reverse("competition-update", kwargs={"pk": self.pk})


class ModelDiscipline(models.Model):
    name = models.CharField(max_length=100, blank=True, unique=True)
    smaller_is_better = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.name}, {self.smaller_is_better}"

    def get_absolute_url(self):
        return reverse("disciplines-list")


class ModelTeam(models.Model):
    name = models.CharField(max_length=50)
    verein = models.CharField(max_length=100)
    startnumber = models.PositiveIntegerField(null=True)

    def get_startnumber(self) -> str:
        if self.startnumber is None:
            return ""
        return str(self.startnumber)

    def get_absolute_url(self):
        return reverse("team-update", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.name} ({self.verein}), Startnummer {self.startnumber}"
    
    class Meta:
        unique_together = ("name", "verein")


class ModelAthlete(models.Model):
    name = models.CharField(max_length=100)
    yob = models.PositiveIntegerField(
        validators=[
            validators.MinValueValidator(1900),
            validators.MaxValueValidator(datetime.datetime.now().year),
        ],
    )
    # to being able to delete teams (while keeping the athletes): nullable, emptyable
    team = models.ForeignKey("ModelTeam", on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return f"{self.name} ({self.yob})"

    def get_absolute_url(self):
        return reverse("teams")
    
    class Meta:
        unique_together = ("name", "yob")


class ModelResult(models.Model):
    discipline = models.ForeignKey("ModelDiscipline", on_delete=models.CASCADE)
    team = models.ForeignKey("ModelTeam", on_delete=models.CASCADE)
    result = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return f"{self.result} ({self.discipline}, {self.team})"

    def clean(self):
        # cleaned_data = super().clean()
        # if cleaned_data:
        if self.result == "":
            return
        if self.discipline.smaller_is_better:
            if not re.search(REGEX_TIME, self.result):
                msg = f"Result {self.result} is not valid (regex: {REGEX_TIME})"
                raise ValidationError(msg)
        else:
            try:
                float(self.result.replace(",", ".").strip())
            except ValueError:
                msg = f"Result {self.result} is not valid (no time)"
                raise ValidationError(msg)
