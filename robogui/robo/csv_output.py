import csv
from typing import List

import django.db.models.query

from robinsoncup.teams import PointsPer
from robo import models


def data_to_response(response):
    _write_to_file_or_response(csv.writer(response, delimiter=","))


def data_to_csv(outfile):
    with open(outfile, "w", encoding="utf-8", newline="") as csvfile:
        _write_to_file_or_response(csv.writer(csvfile, delimiter=","))


def _write_to_file_or_response(writer: csv.writer):
    comp, disc, teams, athletes, results = read_data()
    _write_competition(writer, comp)
    _write_disciplines(writer, disc)
    _write_teams(writer, teams, athletes, results)


def _write_competition(csv_writer: csv.writer, comp):
    csv_writer.writerow([comp.name])
    csv_writer.writerow([comp.date.strftime("%d.%m.%Y")])
    csv_writer.writerow([comp.location])
    csv_writer.writerow(
        ["Wertung über", PointsPer(comp.points_per).name, "(Altersklasse|Alle)"]
    )


def _write_disciplines(csv_writer: csv.writer, disc):
    csv_writer.writerow(
        ["Bewerbe", "1: Kleiner=Besser (sek); 0: Größer=Besser (meter)"]
    )
    for d in disc:
        csv_writer.writerow([d.name, 1 if d.smaller_is_better else 0])


def _write_teams(csv_writer: csv.writer, teams, athletes, results: List[List[str]]):
    csv_writer.writerow(
        [
            "Teams",
            "Verein",
            "Startnummer",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Altersklasse",
            "Gesamtrang",
            "Gesamtpunkte",
            "Leistung1",
            "Punkte1",
            "Leistung...",
            "Punkte...",
        ]
    )
    for athl_cur_team, t, res_cur_team in zip(athletes, teams, results):
        athletes_row = [x for a in athl_cur_team for x in [a.name, a.yob]]
        athletes_row = athletes_row + [""] * (10 - 2 * len(athl_cur_team))
        res_row = [x for r in res_cur_team for x in [r, ""]]
        team_row = (
            [t.name, t.verein, t.get_startnumber()]
            + athletes_row
            + ["", "", ""]
            + res_row
        )
        csv_writer.writerow(team_row)


def read_data():
    comp = _read_comp()
    disc = _read_disc()
    teams = _read_teams()
    athletes = _read_athletes(teams)
    results = _read_results(disc, teams)
    return comp, disc, teams, athletes, results


def _read_comp():
    count_disc = models.ModelCompetition.objects.count()
    if count_disc != 1:
        raise RuntimeError(f"Found {count_disc} competitions. Required is 1")
    comp = models.ModelCompetition.objects.get()
    return comp


def _read_disc():
    return models.ModelDiscipline.objects.all()


def _read_teams():
    return models.ModelTeam.objects.all()


def _read_athletes(teams) -> List[django.db.models.query.QuerySet]:
    return [models.ModelAthlete.objects.filter(team_id=t.id) for t in teams]


def _read_results(disc, teams) -> List[List[str]]:
    out = [["" for i in range(len(disc))] for j in range(len(teams))]
    for row, t in enumerate(teams):
        for col, d in enumerate(disc):
            try:
                cur_result = models.ModelResult.objects.get(
                    team_id=t.id, discipline_id=d.id
                ).result
            except (
                models.ModelResult.DoesNotExist,
                models.ModelResult.MultipleObjectsReturned,
            ):
                pass
            else:
                if cur_result is not None:
                    out[row][col] = cur_result
    return out
