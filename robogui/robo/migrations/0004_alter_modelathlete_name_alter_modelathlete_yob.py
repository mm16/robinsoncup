# Generated by Django 4.0.4 on 2022-07-22 04:11

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("robo", "0003_alter_modeldiscipline_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="modelathlete",
            name="name",
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name="modelathlete",
            name="yob",
            field=models.PositiveIntegerField(
                validators=[
                    django.core.validators.MinValueValidator(1900),
                    django.core.validators.MaxValueValidator(2022),
                ]
            ),
        ),
    ]
