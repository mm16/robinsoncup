from django.forms import ModelForm
from django.forms import widgets

from . import models


class CompetitionForm(ModelForm):
    class Meta:
        model = models.ModelCompetition
        fields = "__all__"


class TeamForm(ModelForm):
    class Meta:
        model = models.ModelTeam
        fields = "__all__"
