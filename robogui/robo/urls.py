from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path(
        "resultsperdiscipline/<int:pk>",
        views.ResultsPerDisciplineView.as_view(),
        name="results-per-discipline",
    ),
    path(
        "resultsperdiscipline/<int:pk>/<str:wertung>/",
        views.ResultsPerDisciplineView.as_view(),
        name="results-per-discipline",
    ),
    path("athletes/", views.AthletesListView.as_view(), name="athletes-list"),
    path(
        "athletes/<int:pk>",
        views.AthletesListPerClubView.as_view(),
        name="athletes-list",
    ),
    path(
        "athlete/add/<int:pk>", views.AthleteCreateView.as_view(), name="athlete-create"
    ),
    path("athlete/<int:pk>/", views.AthleteUpdateView.as_view(), name="athlete-update"),
    path(
        "athlete/<int:pk>/delete/",
        views.AthleteDeleteView.as_view(),
        name="athlete-delete",
    ),
    path("teams/", views.TeamsView.as_view(), name="teams"),
    path("teams/<str:wertung>/", views.TeamsView.as_view(), name="teams"),
    path("team/add/", views.TeamCreateView.as_view(), name="team-add"),
    path("team/<int:pk>", views.TeamUpdateView.as_view(), name="team-update"),
    path("team/<int:pk>/delete/", views.TeamDeleteView.as_view(), name="team-delete"),
    path("out/<str:outputtype>/", views.csv_view, name="out"),
    path("competition/", views.CompetitionListView.as_view(), name="competition-list"),
    path(
        "competition/add/",
        views.CompetitionCreateView.as_view(),
        name="competition-add",
    ),
    path(
        "competition/<int:pk>/",
        views.CompetitionUpdateView.as_view(),
        name="competition-update",
    ),
    path(
        "competition/<int:pk>/delete",
        views.CompetitionDeleteView.as_view(),
        name="competition-delete",
    ),
    path("disciplines/", views.DisciplineListView.as_view(), name="disciplines-list"),
    path(
        "discipline/add/", views.DisciplineCreateView.as_view(), name="discipline-add"
    ),
    path(
        "discipline/<int:pk>/",
        views.DisciplineUpdateView.as_view(),
        name="discipline-update",
    ),
    path(
        "discipline/<int:pk>/delete/",
        views.DisciplineDeleteView.as_view(),
        name="discipline-delete",
    ),
]
