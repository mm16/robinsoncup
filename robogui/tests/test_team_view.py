import datetime

import pytest
from django.test import Client

from robo.models import ModelAthlete
from robo.models import ModelTeam


@pytest.mark.django_db
@pytest.mark.parametrize("wertung", ["", "MINI/", "MEDI/", "MAXI/"])
def test_teams_list(mocker, wertung):
    patched = mocker.patch.object(datetime, "date", mocker.Mock(wraps=datetime.date))
    patched.today.return_value = datetime.date(2018, 1, 1)

    t1 = ModelTeam.objects.create(name="Club1", verein="ATSV", startnumber=44)
    ModelAthlete.objects.create(name="Tom", yob="2009", team=t1)
    c = Client()
    route = f"/robo/teams/{wertung}"

    response = c.get(route)
    assert response.status_code == 200


def test_teams_list_wrong_wertung():
    c = Client()
    route = f"/robo/teams/not_existing/"

    response = c.get(route)
    assert response.status_code == 404


@pytest.mark.django_db
def test_team_add_update_delete():
    c = Client()

    name = "Superteam"
    club = "ATSV"
    stnr = 44
    response = c.post(
        "/robo/team/add/", {"name": name, "verein": club, "startnumber": stnr}
    )
    team = ModelTeam.objects.get(id=1)
    assert team.name == name
    assert response.status_code == 302

    new_name = "Goodteam"
    response = c.post(
        "/robo/team/1", {"name": new_name, "verein": club, "startnumber": stnr}
    )
    team = ModelTeam.objects.get(id=1)
    assert team.name == new_name
    assert response.status_code == 302

    response = c.post("/robo/team/1/delete/")
    assert response.status_code == 302
    assert ModelTeam.objects.count() == 0
