"""Most of the test functions test model internal functions (such as __str__()) to increase test coverage."""
import pytest

from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from robo.models import ModelAthlete
from robo.models import ModelCompetition
from robo.models import ModelDiscipline
from robo.models import ModelResult
from robo.models import ModelTeam


@pytest.mark.django_db
def test_competition_str():
    d = ModelCompetition.objects.create(
        name="Robocup", date="2022-06-16", location="Wien-Stadlau", points_per=1
    )
    assert str(d) == "Robocup, 2022-06-16 (Wien-Stadlau)"


@pytest.mark.django_db
def test_discipline_str():
    d = ModelDiscipline.objects.create(name="Sprint", smaller_is_better=True)
    assert str(d) == "Sprint, True"


@pytest.mark.django_db
def test_team_str():
    t = ModelTeam.objects.create(name="Team1", verein="Club1", startnumber=25)
    assert str(t) == "Team1 (Club1), Startnummer 25"


@pytest.mark.django_db
def test_athlete_str():
    t = ModelTeam(name="T", verein="V", startnumber=1)
    a = ModelAthlete(name="Tom", yob=1986, team=t)
    assert str(a) == "Tom (1986)"


@pytest.mark.django_db
@pytest.mark.parametrize("name,yob", [(None, 1986), ("Tom", None), (None, None)])
def test_athlete_value_missing(name, yob):
    t = ModelTeam.objects.create(name="T", verein="V", startnumber=1)
    with pytest.raises(IntegrityError):
        ModelAthlete.objects.create(name=name, yob=yob, team=t)


@pytest.mark.django_db
@pytest.mark.parametrize("result", ["", "9.79"])
def test_result_str(result):
    d = ModelDiscipline.objects.create(name="Sprint", smaller_is_better=True)
    t = ModelTeam.objects.create(name="T", verein="V", startnumber=1)
    r = ModelResult.objects.create(discipline=d, team=t, result=result)
    assert str(r) == f"{result} (Sprint, True, T (V), Startnummer 1)"
    r.clean()


@pytest.mark.django_db
@pytest.mark.parametrize("result", ["9-79", "foo", "9.44a"])
def test_result_clean_wrong_time_raise(result):
    d = ModelDiscipline.objects.create(name="Sprint", smaller_is_better=True)
    t = ModelTeam.objects.create(name="T", verein="V", startnumber=1)
    r = ModelResult.objects.create(discipline=d, team=t, result=result)
    with pytest.raises(ValidationError, match=r"Result.* is not valid \(regex:.*"):
        r.clean()


@pytest.mark.django_db
@pytest.mark.parametrize("result", ["9-79", "foo", "9.44a", "4:55.12"])
def test_result_clean_wrong_technical_discipline_raise(result):
    d = ModelDiscipline.objects.create(name="Throw", smaller_is_better=False)
    t = ModelTeam.objects.create(name="T", verein="V", startnumber=1)
    r = ModelResult.objects.create(discipline=d, team=t, result=result)
    with pytest.raises(ValidationError, match=r"Result.* is not valid"):
        r.clean()
