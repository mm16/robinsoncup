import pytest

from django.test import Client

from robo.models import ModelAthlete
from robo.models import ModelTeam


@pytest.mark.django_db
def test_list():
    c = Client()
    route = "/robo/athletes/"
    response = c.get(route)

    assert response.status_code == 200


@pytest.mark.django_db
def test_list_per_club():
    t1 = ModelTeam.objects.create(name="t1", verein="ATSV", startnumber=44)
    ModelAthlete.objects.create(name="Lisa", yob=2000, team=t1)

    c = Client()

    response = c.get("/robo/athletes/")
    assert response.status_code == 200

    response = c.get("/robo/athletes/1")
    assert response.status_code == 200

    response = c.get("/robo/athletes/2")
    assert response.status_code == 200


@pytest.mark.django_db
def test_create_update_delete():
    t1 = ModelTeam.objects.create(name="t1", verein="ATSV", startnumber=44)
    c = Client()

    # create
    response = c.post(f"/robo/athlete/add/{t1.id}", {"name": "tom", "yob": 2000})
    assert response.status_code == 302
    athlete = ModelAthlete.objects.get(id=1)
    assert athlete.name == "tom"

    # update
    response = c.post("/robo/athlete/1/", {"name": "mike", "yob": 2000, "team": 1})
    assert response.status_code == 302
    athlete = ModelAthlete.objects.get()
    assert athlete.name == "mike"

    # delete
    response = c.post("/robo/athlete/1/delete/")
    assert response.status_code == 302
    assert ModelAthlete.objects.count() == 0
