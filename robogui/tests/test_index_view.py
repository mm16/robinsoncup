from django.test import Client


def test_get_competition_list():
    c = Client()

    response = c.get("/robo/")
    assert response.status_code == 200
    assert b"Robinsoncup - Startseite" in response.content
