import datetime

import pytest

from django.test import Client

from robo.models import ModelCompetition


@pytest.mark.django_db
def test_get_competition_list():
    c = Client()

    response = c.get("/robo/competition/")
    assert b"Add new competition" in response.content

    ModelCompetition.objects.create(
        name="Compet", date="2022-12-12", location="AT", points_per=1
    )
    response = c.get("/robo/competition/")
    assert b"Add new competition" not in response.content


@pytest.mark.django_db
def test_post_and_get_competition():
    c = Client()

    response = c.get("/robo/competition/1/")
    assert response.status_code == 404  # does not exist

    # create
    response = c.post(
        "/robo/competition/add/",
        {"name": "foo", "date": "2022-12-24", "location": "Wien", "points_per": 1},
    )
    assert response.status_code == 302

    comp = ModelCompetition.objects.get(id=1)
    assert comp.name == "foo"
    assert comp.date == datetime.date(2022, 12, 24)
    assert comp.location == "Wien"
    assert comp.points_per == 1

    # update
    resp = c.post(
        "/robo/competition/1/",
        {"name": "bar", "date": "2022-12-24", "location": "Wien", "points_per": 1},
    )
    assert resp.status_code == 302
    comp = ModelCompetition.objects.get(id=1)
    assert comp.name == "bar"

    # delete
    resp = c.post("/robo/competition/1/delete")
    assert resp.status_code == 302
    assert ModelCompetition.objects.count() == 0
