import pytest
from robo import csv_output
from robo.models import ModelCompetition
from robo.models import ModelDiscipline
from robo.models import ModelTeam
from robo.models import ModelAthlete
from robo.models import ModelResult
from robinsoncup import io_in
from robinsoncup import teams
from robinsoncup.tests import test_io


COMP_NAME = "Robinson-Cup"


def _create_example_data_in_db():
    ModelCompetition.objects.create(
        name="Robinson-Cup",
        date="2022-09-02",
        location="Wien-Stadlau",
        points_per=teams.PointsPer.Altersklasse.value,
    )
    d_sprint = ModelDiscipline.objects.create(name="Sprint", smaller_is_better=True)
    d_vortex = ModelDiscipline.objects.create(
        name="Vortex-Weitwurf", smaller_is_better=False
    )
    d_biathlon = ModelDiscipline.objects.create(
        name="Biathlon-Staffel", smaller_is_better=True
    )
    d_medball = ModelDiscipline.objects.create(
        name="1kg Medizinballstoß", smaller_is_better=False
    )
    d_3erhop = ModelDiscipline.objects.create(
        name="Beidbeiniger 3er-Hopp", smaller_is_better=False
    )

    team = ModelTeam.objects.create(
        name="Die Purgstall Flitzers", verein="UVB Purgstall", startnumber=99
    )
    ModelAthlete.objects.create(name="Olivia Perera", yob=2014, team=team)
    ModelAthlete.objects.create(name="Laurent Wurzer", yob=2014, team=team)
    ModelAthlete.objects.create(name="Gabriel Egger", yob=2015, team=team)
    ModelAthlete.objects.create(name="Alex Brenneis", yob=2013, team=team)
    ModelResult.objects.create(discipline=d_sprint, team=team, result="32.7")
    ModelResult.objects.create(discipline=d_vortex, team=team, result="14.53")
    ModelResult.objects.create(discipline=d_biathlon, team=team, result="01:58.67")
    ModelResult.objects.create(discipline=d_medball, team=team, result="37.6")
    ModelResult.objects.create(discipline=d_3erhop, team=team, result="19.9")

    team = ModelTeam.objects.create(
        name="Riverside Runners", verein="ULC Riverside Mödling", startnumber=5
    )
    ModelAthlete.objects.create(name="Nikola Jörgl", yob=2011, team=team)
    ModelAthlete.objects.create(name="Angelina Maxinkovic", yob=2011, team=team)
    ModelAthlete.objects.create(name="Jakob Huber", yob=2011, team=team)
    ModelAthlete.objects.create(name="David Speckner", yob=2011, team=team)
    ModelResult.objects.create(discipline=d_sprint, team=team, result="28.9")
    ModelResult.objects.create(discipline=d_vortex, team=team, result="23.83")
    ModelResult.objects.create(discipline=d_biathlon, team=team, result="1:31.77")
    ModelResult.objects.create(discipline=d_medball, team=team, result="54.5")
    ModelResult.objects.create(discipline=d_3erhop, team=team, result="21.7")

    team = ModelTeam.objects.create(name="Young Sprinters", verein="ATSV OMV Auersthal")
    ModelAthlete.objects.create(name="Marlen Lexa", yob=2013, team=team)
    ModelAthlete.objects.create(name="Niclas Bayer", yob=2014, team=team)
    ModelAthlete.objects.create(name="Emmanuel Egolum", yob=2014, team=team)
    ModelAthlete.objects.create(name="Benjamin Freiberg", yob=2013, team=team)
    ModelResult.objects.create(discipline=d_sprint, team=team, result="32.2")
    ModelResult.objects.create(discipline=d_vortex, team=team, result="13.6")
    ModelResult.objects.create(discipline=d_biathlon, team=team, result="01:57.71")
    ModelResult.objects.create(discipline=d_medball, team=team, result="32.3")
    ModelResult.objects.create(discipline=d_3erhop, team=team, result="17.4")

    team = ModelTeam.objects.create(
        name="SVS1", verein="SVS-Leichtathletik", startnumber=3
    )
    ModelAthlete.objects.create(name="Florian Volek", yob=2012, team=team)
    ModelAthlete.objects.create(name="Fiona Gepp", yob=2011, team=team)
    ModelAthlete.objects.create(name="Gloria Zöttl", yob=2012, team=team)
    ModelResult.objects.create(discipline=d_sprint, team=team, result="32")
    ModelResult.objects.create(discipline=d_vortex, team=team, result="22.03")
    ModelResult.objects.create(discipline=d_biathlon, team=team, result="2:00.33")
    ModelResult.objects.create(discipline=d_medball, team=team, result="47.7")
    ModelResult.objects.create(discipline=d_3erhop, team=team, result="19.4")

    team = ModelTeam.objects.create(
        name="Die drei Musketiere", verein="ATSV OMV Auersthal"
    )
    ModelAthlete.objects.create(name="Hanna Kastner", yob=2014, team=team)
    ModelAthlete.objects.create(name="Raphael Ralbovsky", yob=2015, team=team)
    ModelAthlete.objects.create(name="Max Mikolajczyk", yob=2015, team=team)
    ModelResult.objects.create(discipline=d_sprint, team=team, result="34.4")
    ModelResult.objects.create(discipline=d_vortex, team=team, result="19.85")
    ModelResult.objects.create(discipline=d_biathlon, team=team, result="02:35.56")
    ModelResult.objects.create(discipline=d_medball, team=team, result="35.5")
    ModelResult.objects.create(discipline=d_3erhop, team=team, result="18.1")

    team = ModelTeam.objects.create(
        name="Die 4 Muchachos", verein="UVB Purgstall", startnumber=15
    )
    ModelAthlete.objects.create(name="Elias Wurzer", yob=2012, team=team)
    ModelAthlete.objects.create(name="Teresa Egger", yob=2012, team=team)
    ModelAthlete.objects.create(name="Markus Luger", yob=2011, team=team)
    ModelAthlete.objects.create(name="Paul Weber", yob=2012, team=team)
    ModelResult.objects.create(discipline=d_sprint, team=team, result="31.9")
    ModelResult.objects.create(discipline=d_vortex, team=team, result="21.48")
    ModelResult.objects.create(discipline=d_biathlon, team=team, result="1:44.93")
    ModelResult.objects.create(discipline=d_medball, team=team, result="55.1")
    ModelResult.objects.create(discipline=d_3erhop, team=team, result="22.3")


@pytest.mark.django_db
def test_to_csv(tmp_path):
    _create_example_data_in_db()
    output_file = tmp_path / "output.csv"
    csv_output.data_to_csv(output_file)
    expected_same_file = "../example.csv"
    test_io.assert_csvfile_only_filled_emptys(output_file, expected_same_file)
    test_io.assert_csvfile_only_filled_emptys(expected_same_file, output_file)


@pytest.mark.django_db
def test_to_csv_single_result_missing(tmp_path):
    _create_example_data_in_db()
    output_file = tmp_path / "output.csv"
    ModelResult.objects.filter(discipline__id=2, team__id=2).delete()
    csv_output.data_to_csv(output_file)
    expected_same_file = "../example.csv"
    test_io.assert_csvfile_only_filled_emptys(output_file, expected_same_file)


def _add_comp():
    ModelCompetition.objects.create(
        name=COMP_NAME,
        date="2022-09-02",
        location="Wien-Stadlau",
        points_per=teams.PointsPer.Altersklasse.value,
    )


def _add_single_disc():
    ModelDiscipline.objects.create(name="Sprint", smaller_is_better=True)


def _add_single_team():
    return ModelTeam.objects.create(
        name="Die Purgstall Flitzers", verein="UVB Purgstall", startnumber=99
    )


@pytest.mark.django_db
def test_only_competition_to_csv(tmp_path):
    _add_comp()

    output_file = tmp_path / "output.csv"
    csv_output.data_to_csv(output_file)

    comp, disc, team_ = io_in.read_input(output_file)
    assert comp.name == COMP_NAME
    assert not disc
    assert not team_


@pytest.mark.django_db
def test_single_discipline_to_csv(tmp_path):
    output_file = tmp_path / "output.csv"

    _add_single_disc()

    with pytest.raises(RuntimeError, match="Found 0 competitions. Required is 1.*"):
        csv_output.data_to_csv(output_file)

    _add_comp()
    csv_output.data_to_csv(output_file)

    comp, disc, team_ = io_in.read_input(output_file)
    assert len(disc) == 1
    assert disc[0].name == "Sprint"
    assert disc[0].smaller_is_better is True
    assert not team_


@pytest.mark.parametrize("add_athlete,exp_number_athl", [(True, 1), (False, 0)])
@pytest.mark.django_db
def test_single_team_no_athletes_to_csv(tmp_path, add_athlete, exp_number_athl):
    output_file = tmp_path / "output.csv"
    _add_comp()
    team = _add_single_team()
    if add_athlete:
        ModelAthlete.objects.create(name="Elias Wurzer", yob=2012, team=team)
    csv_output.data_to_csv(output_file)

    comp, disc, team_ = io_in.read_input(output_file)
    assert len(team_) == 1
    assert team_[0].startnumber == 99
    assert len(team_[0].athletes) == exp_number_athl
