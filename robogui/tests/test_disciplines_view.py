import pytest

from django.test import Client

from robo.models import ModelDiscipline


@pytest.mark.django_db
def test_disciplines_list():
    c = Client()
    route = "/robo/disciplines/"

    resp = c.get(route)
    assert resp.status_code == 200


@pytest.mark.django_db
def test_discipline():
    c = Client()

    # create
    response = c.post(
        "/robo/discipline/add/", {"name": "Sprint", "smaller_is_better": True}
    )
    assert response.status_code == 302

    disc = ModelDiscipline.objects.get(id=1)
    assert disc.name == "Sprint"
    assert disc.smaller_is_better is True

    # get
    route = "/robo/discipline/1/"
    resp = c.get(route)
    assert resp.status_code == 200

    # update
    data = {"name": "60m-Sprint", "smaller_is_better": True}
    resp = c.post(route, data)
    assert resp.status_code == 302

    disc = ModelDiscipline.objects.get(pk=1)
    assert disc.name == "60m-Sprint"
    assert disc.smaller_is_better is True

    # delete
    route = "/robo/discipline/1/delete/"
    resp = c.post(route)
    assert resp.status_code == 302
    assert ModelDiscipline.objects.count() == 0
