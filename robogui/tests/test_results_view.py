import datetime
import pytest

from django.test import Client

from robinsoncup.teams import WERTUNGEN

from robo.models import ModelAthlete
from robo.models import ModelDiscipline
from robo.models import ModelResult
from robo.models import ModelTeam


@pytest.fixture
def teams(mocker):
    cur_year = datetime.date.today().year
    t1 = ModelTeam.objects.create(name="Mini-Team", verein="ATSV", startnumber=22)
    ModelAthlete.objects.create(name="Mini-Athlete", yob=cur_year - 8, team=t1)
    t2 = ModelTeam.objects.create(name="Maxi-Team", verein="ULC", startnumber=45)
    ModelAthlete.objects.create(name="Maxi-Athlete", yob=cur_year - 10, team=t2)
    return t1, t2


@pytest.fixture
def disciplines():
    d1 = ModelDiscipline.objects.create(name="Sprint", smaller_is_better=True)
    d2 = ModelDiscipline.objects.create(name="Speer", smaller_is_better=False)
    return d1, d2


@pytest.mark.django_db
def test_results_pages(disciplines, teams):
    c = Client()
    for d in disciplines:
        route = f"/robo/resultsperdiscipline/{d.id}"
        response = c.get(route)
        assert response.status_code == 200
        for w in WERTUNGEN.keys():
            route = f"/robo/resultsperdiscipline/{d.id}/{w}/"
            response = c.get(route)
            assert response.status_code == 200


@pytest.mark.django_db
def test_get_request_creates_empty_results(disciplines, teams):
    c = Client()
    for d in disciplines:
        route = f"/robo/resultsperdiscipline/{d.id}"
        c.get(route)

    all_results = ModelResult.objects.all()
    assert len(all_results) == 4
    for result in all_results:
        assert result.result == ""
    assert all_results[0].discipline.id == disciplines[0].id
    assert all_results[1].discipline.id == disciplines[0].id
    assert all_results[2].discipline.id == disciplines[1].id
    assert all_results[3].discipline.id == disciplines[1].id


@pytest.mark.django_db
def test_add_results_to_all_teams():
    cur_year = datetime.date.today().year
    d1 = ModelDiscipline.objects.create(name="Sprint", smaller_is_better=True)
    t = [
        ModelTeam.objects.create(name="Mini-Team", verein="ATSV", startnumber=22),
        ModelTeam.objects.create(name="Mini-Team2", verein="ATSV", startnumber=81),
        ModelTeam.objects.create(name="Mini-Team3", verein="ATSV", startnumber=303),
    ]
    ModelAthlete.objects.create(name="Mini-Athlete", yob=cur_year - 12, team=t[0])
    ModelAthlete.objects.create(name="Mini-Athlete2", yob=cur_year - 12, team=t[1])
    ModelAthlete.objects.create(name="Mini-Athlete3", yob=cur_year - 12, team=t[2])

    c = Client()
    route = f"/robo/resultsperdiscipline/{d1.id}"

    # let get request create empty values
    response = c.get(route)
    assert response.status_code == 200
    assert ModelResult.objects.count() == 3
    for tt in t:
        r = ModelResult.objects.get(team=tt)
        assert r.result == ""
        assert r.team == tt
        assert r.discipline == d1

    # create results
    data = {
        "form-TOTAL_FORMS": "3",
        "form-INITIAL_FORMS": "3",
        "form-MAX_NUM_FORMS": "3",
        "form-0-result": "1.1",
        "form-0-id": "1",
        "form-1-result": "1.2",
        "form-1-id": "2",
        "form-2-result": "1.3",
        "form-2-id": "3",
    }
    response = c.post(route, data)
    assert response.status_code == 200
    assert ModelResult.objects.get(team=t[0], discipline=d1).result == "1.1"
    assert ModelResult.objects.get(team=t[1], discipline=d1).result == "1.2"
    assert ModelResult.objects.get(team=t[2], discipline=d1).result == "1.3"


@pytest.mark.parametrize("team_ind,wertung", [(0, "MINI"), (1, "MAXI")])
@pytest.mark.django_db
def test_add_results_to_subset_of_teams(teams, disciplines, team_ind, wertung):
    current_team = teams[team_ind]
    d = disciplines[0]

    c = Client()
    route = f"/robo/resultsperdiscipline/{d.id}/{wertung}/"

    # let get request create empty values
    response = c.get(route)
    assert response.status_code == 200
    assert ModelResult.objects.count() == 1
    r = ModelResult.objects.get(team=current_team)
    assert r.result == ""
    assert r.team == current_team
    assert r.discipline == d

    # create results
    data = {
        "form-TOTAL_FORMS": "1",
        "form-INITIAL_FORMS": "1",
        "form-MAX_NUM_FORMS": "1",
        "form-0-result": "9.9",
        "form-0-id": f"{r.id}",
    }
    response = c.post(route, data)
    assert response.status_code == 200
    assert ModelResult.objects.get(team=current_team, discipline=d).result == "9.9"
