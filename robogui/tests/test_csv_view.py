import os

import pytest

from django.test import Client

from robo.models import ModelCompetition
from robo import views


@pytest.fixture
def out_files_mock(mocker, tmpdir):
    out_files_new = {
        "csv": os.path.join(tmpdir, "0_input_auto-generated-from-robogui.csv"),
        "startlist": os.path.join(
            tmpdir, "1_startlist_auto-generated-from-robogui.pdf"
        ),
        "points": os.path.join(tmpdir, "2_points_auto-generated-from-robogui.csv"),
        "results": os.path.join(tmpdir, "3_results_auto-generated-from-robogui.pdf"),
        "certificates": os.path.join(
            tmpdir, "4_certificates_auto-generated-from-robogui.pdf"
        ),
    }
    mocker.patch("robo.views.OUT_FILES", out_files_new)
    yield out_files_new


def test_wrong_type():
    c = Client()
    response = c.get("/robo/out/not-existing/")
    assert response.status_code == 200
    exp_error_msg = b"Request str not allowed. Allowed:"
    assert exp_error_msg in response.content


@pytest.mark.django_db
def test_raise_in_backend_to_error_message():
    c = Client()
    response = c.get("/robo/out/csv/")  # fails since no data
    exp_error_msg = b"Error: Found 0 competitions. Required is 1"
    assert exp_error_msg in response.content


@pytest.mark.django_db
def test_csv():
    ModelCompetition.objects.create(
        name="Compet", date="2022-12-12", location="AT", points_per=1
    )
    c = Client()
    route = "/robo/out/csv/"
    response = c.get(route)

    assert response.status_code == 200
    assert b"2 files created: " in response.content


@pytest.mark.django_db
def test_startlist(out_files_mock):
    assert not os.path.exists(out_files_mock["csv"])
    ModelCompetition.objects.create(
        name="Compet", date="2022-12-12", location="AT", points_per=1
    )
    c = Client()
    route = "/robo/out/startlist/"

    response = c.get(route)
    assert response.status_code == 200
    assert b"create CSV first" in response.content

    c.get("/robo/out/csv/")
    assert os.path.exists(out_files_mock["csv"])
    response = c.get(route)
    assert os.path.exists(out_files_mock["startlist"])
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "application/pdf"
    assert b"ReportLab Generated PDF document" in response.content


@pytest.mark.django_db
def test_results(out_files_mock):
    ModelCompetition.objects.create(
        name="Compet", date="2022-12-12", location="AT", points_per=1
    )
    c = Client()
    route = "/robo/out/results/"

    response = c.get(route)
    assert response.status_code == 200
    assert b" create csv first" in response.content

    c.get("/robo/out/csv/")  # create required csv files
    response = c.get(route)
    assert response.status_code == 200
    assert b"Results, certificates created: " in response.content
