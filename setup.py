from setuptools import setup

setup(
    name="robinsoncup",
    version="0.1",
    description="Robinsoncup",
    url="http://gitlab.com/mm16/robinsoncup",
    author="MM",
    author_email="mm@example.com",
    license="MIT",
    packages=["robinsoncup"],
    zip_safe=False,
)
