# Robinsoncup

Program to run a Robinson-Cup competition.


# Setup instructions

    git clone https://gitlab.com/mm16/robinsoncup.git
    cd robinsoncup

## Install
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    pip install wheel  # might be necessary on Ubuntu
    pip install -e .

# Use program

The program can be run either using the web-GUI or the command-line directly:

## Via Web-GUI

    cd robogui (where manage.py is)
    # if very first time after installation: create (empty) database
    python manage.py makemigrations
    python manage.py migrate

    python manage.py runserver
    # go to 127.0.0.1:8000/robo in browser

## Input formats

* Times: Either of
  * Float seconds (example 5.2)
  * mm:ss.hh (example: 5:10.43, 1:12.3)
* Distance
  * Float meters (examples: 28.5, 26, 55.12)

### Hints

* (Up-to-date!) CSV is required also for results!
* If pdf (startlist, result) empty (or not as expected): Ensure to create up-to-date csv first
* The db is stored in: `robogui/db.sqlite3`
* The "Output"-buttons (CSV, Startliste, Ergebnisse/Urkunden) create following files in `robinsoncup/`:
  * 0_input_auto-generated-from-robogui.csv
  * 1_startlist_auto-generated-from-robogui.pdf
  * 2_points_auto-generated-from-robogui.csv
  * 3_results_auto-generated-from-robogui.pdf
  * 4_certificates_auto-generated-from-robogui.pdf
* Empty certificates (ie without logos for faster printing)
  * Change TEXT_ONLY_CERTIFICATES value in `robogui/robo/views.py`
* Personal note: For Kyocera printer:
  * Setting printer:
    * Paper A5-R (if paper is "like A4"/portrait in printer)
    * Papierzufuhrart: Fest
    * Auto Kassette wechsel: Aus
  * Setting PC
    * Don't use Adobe Acrobat
    * Use (eg) Sumatra PDF
    * Paper: A5
    * Portrait mode

## Via command-line

The data (competition details, disciplines, athletes, results) must be written 
to a .csv-file with a proper format (refer to example.csv). Delimiter must be ','.

Requirements for running the below commands are

    cd robinsoncup
    source venv/bin/activate  # Activate virtual environment (command for linux)
    ...  # Do stuff
    deactivate  # Deactivate virtual environment

All following commands start an argparse script. The options of each can be
seen in the help of each function, such as:

    python <script_name> -h
    python to_startlist.py -h

### Print startlist
Following creates startlist.pdf

    python to_startlist.py example.csv

### Get points/ranks

The points and ranks are calculated by to_points.py. This script creates a new
.csv-file with same data as input plus points and ranks:

    python to_points.py example.csv

### Create results pdf

Following command creates results.pdf and certificates.pdf. It requires
points.csv in the same folder.

    python to_results.py

# Run tests

    # Run tests of package robinsoncup
    cd robinsoncup
    python -m pytest

    # Run tests of django app
    cd robogui  # where manage.py is
    pytest

# Help/errors

* ModuleNotFoundError: No module named 'reportlab'
  --> Activate venv (see under Install)

* (Some) total ranks missing in points.csv (output of to_points)
  --> Teams are equal (same name, club, athletes / probably copied from other line in input, eg example.csv)
* Error with strange encoding, eg when running tests (on Windows): All files should be encoded in UTF-8. Windows usually uses ANSI as default: Be sure to convert to UTF-8.
