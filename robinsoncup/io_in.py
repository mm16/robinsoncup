import csv
import datetime
from typing import List
from typing import Tuple
from typing import Union

from robinsoncup import competition
from robinsoncup import disciplines
from robinsoncup import io_base
from robinsoncup import teams


"""
Requirements for csv file
-------------------------
* Separated with ,
* Competition: Three rows
  * Name (str)
  * Date (d.m.yyyy or dd.mm.yyyy)
  * Location (str)
* Row 4 (ind3) = Header of Bewerbe (content does not matter)
* Rows of Bewerbe (first col: name; second col: 1|0 (1: smaller is better ==> seconds)
* Following line must be "Teams" in first col (further can be headers)
* Team lines (one line for each team)
  * col1 (ind0) = Team name (str)
  * col2 (ind1) = Club (str)
  * col3 (ind2) = Startnummer (int)
  * col4-13 are 5 athletes (name, yob)
  * col14: Altersklasse (str; will be calculated by python script)
  * col15: total rank (will be calculated by python script)
  * col16: total points (will be caclulated by python script)
  * col17-??: result, rank of each discipline (nr. of disc*2 = nr of cols)
              ranks will be calculated by python script
"""


def read_input(file):
    with open(file, encoding="utf-8") as csvfile:
        return _read_buffer(csvfile)


def _read_buffer(
    buf,
) -> Tuple[competition.Competition, List[disciplines.Discipline], List[teams.Team]]:
    """buf is opened file or buffer like io.StringIO"""
    rows = csv.reader(buf, delimiter=",")
    comp_lines = []
    disc_lines = []
    team_lines = []
    already_at_teams_rows = False
    for ind, row in enumerate(rows):
        if ";" in row[0] and "," not in row[0]:
            raise RuntimeError("Expecting , as delimiter and not ;")
        if ind <= 3:
            comp_lines.append(row)
        if ind == 4:
            # Bewerbe
            continue
        if ind > 4:
            if already_at_teams_rows:
                team_lines.append(row)
            else:
                if row[0] == "Teams":
                    already_at_teams_rows = True
                    continue
                else:
                    disc_lines.append(row)

    comp = _read_competition(comp_lines)
    disc = _read_disciplines(disc_lines)
    team = _read_teams(team_lines, disc)
    return comp, disc, team


def _read_competition(rows: List[List[str]]) -> competition.Competition:
    name = rows[0][0]
    date = datetime.datetime.strptime(rows[1][0], "%d.%m.%Y")
    location = rows[2][0]
    assert (
        len(rows) > 3 and rows[3][0] == "Wertung über"
    ), f"Competition must contain 'Wertung über'"
    if rows[3][1] not in {"Altersklasse", "Alle"}:
        raise ValueError(f"Value {rows[3][1]} must one of Altersklasse|Alle")
    points_per = teams.PointsPer[rows[3][1]]
    return competition.Competition(name, date, location, points_per=points_per)


def _read_disciplines(rows: List[str]) -> List[disciplines.Discipline]:
    return [disciplines.Discipline(r[0], bool(int(r[1]))) for r in rows]


def _read_teams(
    rows: List[List[str]], d: List[disciplines.Discipline]
) -> List[teams.Team]:
    return [_read_team(row, d) for row in rows]


def _read_team(row: List[str], d: List[disciplines.Discipline]) -> teams.Team:
    name = row[0].strip()
    club = row[1].strip()
    try:
        startnumber = int(row[2].strip())
    except ValueError:
        startnumber = None
    t = teams.Team(name, club, startnumber=startnumber)
    for athlete_number in (0, 1, 2, 3, 4):
        name = row[3 + athlete_number * 2].strip()
        if not name:
            break
        yob = int(row[4 + athlete_number * 2])
        t.add_athlete(teams.Athlete(name, yob))
    try:
        t.total_rank = int(row[14])
    except ValueError:
        # no total rank in row
        pass
    for ind, dd in enumerate(d):
        res = io_base.parse_result(row[16 + 2 * ind], dd.smaller_is_better)
        if res:
            t.add_result(dd, res)
        try:
            points = row[17 + 2 * ind]
            if points:
                t.add_points(dd, int(points))
        except IndexError:
            pass
    return t
