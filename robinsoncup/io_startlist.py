import datetime
from typing import List

from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import PageBreak
from reportlab.platypus import Paragraph
from reportlab.platypus import SimpleDocTemplate
from reportlab.platypus import Table

from robinsoncup import teams


class Startlist:
    def __init__(
        self,
        out_file,
        comp_name: str,
        date: datetime.datetime,
        location: str,
        disc_names: List[str],
        team: List[teams.Team],
    ):
        self.out_file = out_file
        self.comp_name = comp_name
        self.date = date
        self.location = location
        self.disc_names = disc_names
        self.team = team
        self.pagesize = A4

    def create(self):
        """Creates the pdf report."""
        elements = self._get_elements()

        doc = SimpleDocTemplate(self.out_file, pagesize=self.pagesize)
        doc.build(
            elements, onFirstPage=self._create_header, onLaterPages=self._create_header
        )

    def _create_header(self, cv, doc):
        cv.saveState()

        text = f"STARTLISTE - {self.comp_name}"
        y = self.pagesize[1] - 20
        x = self.pagesize[0] / 2
        cv.drawCentredString(x, y, text)
        text = f"{self.date.strftime('%d.%m.%Y')} ({self.location})"
        # from one line to next: -10 ... very close to each other
        y = self.pagesize[1] - 33
        cv.drawCentredString(x, y, text)

        now = datetime.datetime.now()
        text = f"Printed: {now.strftime('%d.%m.%Y (%H:%M:%S)')}"
        cv.setFontSize(8)
        cv.drawCentredString(x, 20, text)
        cv.restoreState()

    def _get_elements(self):
        sorted_wertungen = sorted(
            teams.WERTUNGEN.keys(), key=lambda x: x[1:], reverse=True
        )
        all_wertungen = set((t.get_wertung(self.date.year) for t in self.team))

        styles = getSampleStyleSheet()
        styleN = styles["Normal"]
        styleN.fontSize = 14
        styleN.spaceAfter = 14
        styleN.leftIndent = -34
        elements = []
        for w in sorted_wertungen:
            if w not in all_wertungen:
                continue
            for d in self.disc_names:
                elements.append(Paragraph(f"{w}-Wertung: {d}", styleN))
                teams_cur_wertung = [
                    t for t in self.team if t.get_wertung(self.date.year) == w
                ]
                elements.append(self._get_table(teams_cur_wertung))
                elements.append(PageBreak())
        return elements

    def _get_table(self, teams_cur_wertung):
        """Called from _get_elements()"""
        # styles = getSampleStyleSheet()
        # styleN = styles["Normal"]
        # styleN.wordWrap = "CJK"
        # disc_cells = [Paragraph(d, styleN) for d in self.disc_names]
        data = [["Team", "Athleten", "", "St-Nr.", "Leistung", "Rang"]]
        for t in teams_cur_wertung:
            data.extend(_team_to_startlist_row(t))

        # add two empty rows for manual writing
        nr_empty_rows_add = 2
        for _ in range(nr_empty_rows_add):
            data.extend(_team_to_startlist_row(None))

        style_list = [
            ("GRID", (0, 1), (-1, -1), 0.5, colors.grey),
            ("ALIGN", (0, 0), (-1, 0), "CENTER"),
            ("ALIGN", (2, 1), (-1, -1), "CENTER"),
            ("VALIGN", (3, 1), (3, -1), "MIDDLE"),
            ("FONTSIZE", (3, 1), (3, -1), 18),
        ]
        for ind_team in range(len(teams_cur_wertung) + nr_empty_rows_add):
            style_list.append(
                ("SPAN", (3, 5 * ind_team + 1), (3, 5 * ind_team + 1 + 4))
            )
            style_list.append(
                ("SPAN", (4, 5 * ind_team + 1), (4, 5 * ind_team + 1 + 4))
            )
            style_list.append(
                ("SPAN", (5, 5 * ind_team + 1), (5, 5 * ind_team + 1 + 4))
            )

        t = Table(
            data,
            colWidths=[5 * cm, 5 * cm, 1 * cm, 2 * cm, 5 * cm, 2 * cm],
            style=[*style_list],
        )
        return t


def _team_to_startlist_row(t: teams.Team) -> List[List[str]]:
    if t is None:
        return [["", "", "", "", "", ""]] * 5
    out = [["", a.name, str(a.yob), t.get_startnumber(), "", ""] for a in t.athletes]
    for k in range(5 - len(out)):
        out.append(["", "", "", "", "", ""])
    out[1][0] = t.name
    out[2][0] = t.verein
    return out
