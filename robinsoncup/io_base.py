import os
import platform
import quantities as pq
import re
import subprocess


REGEX_TIME = r"^\s*((?P<minutes>\d{0,2}):)?(?P<seconds>\d+[,.]?\d{0,2})\s*$"


def results_to_string(res, smaller_is_better):
    """
    Examples
    --------
    >>> results_to_string(None, True)
    ''
    """
    if res is None:
        return ""
    if smaller_is_better:
        return _result_to_string_time(res)
    return _result_to_string_notime(res)


def _result_to_string_notime(res: pq.Quantity) -> str:
    """
    Examples
    --------
    >>> _result_to_string_notime(32.7 * pq.m)
    '32.70'
    >>> _result_to_string_notime(2 * pq.m)
    '2.00'
    """
    return f"{res.rescale(pq.m).magnitude:0.2f}"


def _result_to_string_time(res: pq.Quantity):
    """
    Examples
    --------
    >>> _result_to_string_time(118.67 * pq.s)
    '1:58.67'
    >>> _result_to_string_time(58.67 * pq.s)
    '58.67'
    >>> _result_to_string_time(120.14 * pq.s)
    '2:00.14'
    >>> _result_to_string_time(9.79 * pq.s)
    '9.79'
    """
    if res >= 60 * pq.s:
        mins = res.rescale(pq.s).magnitude // 60
        mins_str = f"{mins:1.0f}:"
        secs_format = "05.2f"
    else:
        mins = 0
        mins_str = ""
        secs_format = "0.2f"
    remaining_secs = res.rescale(pq.s).magnitude - mins * 60
    return f"{mins_str}{remaining_secs:{secs_format}}"


def parse_result(s: str, is_time: bool):
    if is_time:
        return _parse_result_time(s)
    return _parse_result_notime(s)


def _parse_result_time(s: str) -> pq.Quantity:
    """
    Examples
    --------
    >>> _parse_result_time("1:15.4")
    array(75.4) * s
    >>> _parse_result_time("15.4")
    array(15.4) * s
    >>> _parse_result_time("15")
    array(15.) * s
    >>> _parse_result_time("125.4")
    array(125.4) * s
    >>> _parse_result_time("  00:15,4  ")
    array(15.4) * s
    >>> _parse_result_time("")
    >>> _parse_result_time("abc")
    >>> _parse_result_time("a  1:15.4")
    """
    match = re.search(REGEX_TIME, s)
    if not match:
        return None
    seconds = float(match.group("seconds").replace(",", "."))
    try:
        return (float(match.group("minutes")) * 60 + seconds) * pq.s
    except TypeError:
        return seconds * pq.s


def _parse_result_notime(s: str) -> pq.Quantity:
    """
    Examples
    --------
    >>> _parse_result_notime("13.5")
    array(13.5) * m
    >>> _parse_result_notime("13,5")
    array(13.5) * m
    >>> _parse_result_notime("  05 ")
    array(5.) * m
    >>> _parse_result_notime("")
    >>> _parse_result_notime("   ")
    >>> _parse_result_notime("abc")
    """
    try:
        return float(s.replace(",", ".").strip()) * pq.m
    except ValueError:
        # not a valid number
        return None


def open_file_in_os(filepath):
    if platform.system() == "Darwin":  # macOS
        subprocess.call(("open", filepath))
    elif platform.system() == "Windows":  # Windows
        os.startfile(filepath)
    else:  # linux variants
        subprocess.call(("xdg-open", filepath))
