import datetime
import os
from typing import List

from reportlab.lib.pagesizes import A4
from reportlab.lib.pagesizes import A5
from reportlab.lib.pagesizes import landscape
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.pdfgen import canvas
from reportlab.platypus import PageBreak
from reportlab.platypus import Paragraph
from reportlab.platypus import SimpleDocTemplate
from reportlab.platypus import Table

from robinsoncup import disciplines
from robinsoncup import io_base
from robinsoncup import teams


class Report:
    def __init__(
        self,
        out_file,
        comp_name: str,
        date: datetime.datetime,
        location: str,
        team: List[teams.Team],
    ):
        dirname = os.path.dirname(__file__)
        self.out_file = out_file
        self.comp_name = comp_name
        self.date = date
        self.location = location
        self.team = team
        self.logo = os.path.join(dirname, "data/robinson-cup_300x94.png")
        self.pikto_logo = os.path.join(dirname, "data/pikto_and_logo_alpha9.png")

        self._ensure_total_ranks_exist()

    def _ensure_total_ranks_exist(self):
        # check if we have numeric values for all ranks and points
        for t in self.team:
            assert 0 < t.total_rank < 9999999


class Certificates(Report):
    def __init__(
        self,
        out_file,
        comp_name: str,
        date: datetime.datetime,
        location: str,
        team: List[teams.Team],
        text_only: bool = False,
    ):
        super().__init__(out_file, comp_name, date, location, team)
        self.text_only = text_only
        self.pagesize = A5

    def _get_elements(self, cv: canvas.Canvas):
        sorted_wertungen = sorted(
            teams.WERTUNGEN.keys(), key=lambda x: x[1:], reverse=True
        )
        all_wertungen = set((t.get_wertung(self.date.year) for t in self.team))

        for w in sorted_wertungen:
            if w not in all_wertungen:
                continue
            cur_teams = [t for t in self.team if t.get_wertung(self.date.year) == w]
            for t in sorted(
                cur_teams,
                key=lambda x: x.get_total_points(),
            ):

                if not self.text_only:
                    cv.drawImage(
                        self.logo,
                        110,
                        438,
                        width=200,
                        preserveAspectRatio=True,
                        mask="auto",
                    )
                    cv.drawImage(
                        self.pikto_logo,
                        40,
                        -240,
                        width=350,
                        preserveAspectRatio=True,
                        mask="auto",
                    )

                cv.setFontSize(10)
                text = "Das Team"
                y = self.pagesize[1] - 180
                x = self.pagesize[0] / 2
                cv.drawCentredString(x, y, text)

                y -= 30
                cv.setFontSize(15)
                cv.drawCentredString(x, y, f"{t.name}")
                y -= 20
                cv.setFontSize(15)
                cv.drawCentredString(x, y, f"({t.verein})")

                cv.setFontSize(10)
                y -= 30
                cv.drawCentredString(x, y, "mit")
                y -= 10
                for a in t.athletes:
                    y -= 20
                    cv.setFontSize(15)
                    cv.drawCentredString(x, y, f"{a.name}")

                cv.setFontSize(10)
                y = y - 30
                cv.drawCentredString(x, y, "erreichte beim")

                y -= 30
                cv.setFontSize(15)
                cv.drawCentredString(x, y, f"{self.comp_name} ({w})")

                y -= 20
                cv.setFontSize(15)
                date_str = f"{self.date.day}.{self.date.month}.{self.date.year}"
                cv.drawCentredString(x, y, f"in {self.location} am {date_str}")

                y -= 30
                cv.setFontSize(15)
                cv.drawCentredString(x, y, f"mit {t.get_total_points()} Punkten den")

                y -= 45
                cv.setFontSize(25)
                cv.drawCentredString(x, y, f"{t.total_rank}. Platz")
                cv.showPage()

    def create(self):
        cv = canvas.Canvas(self.out_file)
        cv.setPageSize(self.pagesize)

        self._get_elements(cv)

        cv.save()


class Results(Report):
    def __init__(
        self,
        out_file,
        comp_name: str,
        date: datetime.datetime,
        location: str,
        disc_names: List[str],
        team: List[teams.Team],
    ):
        super().__init__(out_file, comp_name, date, location, team)
        assert len(disc_names) == len(set(disc_names)), "disc_names are not unique"
        self.disc_names = disc_names
        self.pagesize = landscape(A4)

    def create(self):
        """Creates the pdf report."""
        elements = self._get_elements()

        doc = SimpleDocTemplate(self.out_file, pagesize=self.pagesize)
        doc.build(
            elements, onFirstPage=self._create_header, onLaterPages=self._create_header
        )

    def _get_elements(self):
        sorted_wertungen = sorted(
            teams.WERTUNGEN.keys(), key=lambda x: x[1:], reverse=True
        )
        all_wertungen = set((t.get_wertung(self.date.year) for t in self.team))

        elements = []
        for w in sorted_wertungen:
            if w not in all_wertungen:
                continue
            teams_cur_wertung = [
                t for t in self.team if t.get_wertung(self.date.year) == w
            ]
            elements.append(self._get_table(teams_cur_wertung, w))
            elements.append(PageBreak())
        return elements

    def _create_header(self, cv: canvas.Canvas, doc: SimpleDocTemplate):
        cv.saveState()

        cv.drawImage(
            self.logo, 80, 510, width=200, preserveAspectRatio=True, mask="auto"
        )
        text = f"ERGEBNISBERICHT - {self.comp_name}"
        y = self.pagesize[1] - 20
        x = self.pagesize[0] / 2
        cv.drawCentredString(x, y, text)
        text = f"{self.date.strftime('%d.%m.%Y')} ({self.location})"
        # from one line to next: -10 ... very close to each other
        y = self.pagesize[1] - 33
        cv.drawCentredString(x, y, text)

        now = datetime.datetime.now()
        text = f"Printed: {now.strftime('%d.%m.%Y (%H:%M:%S)')}"
        cv.setFontSize(8)
        cv.drawCentredString(x, 20, text)
        cv.restoreState()

    def _get_table(self, teams_cur_wertung, wertung_name: str):
        """Called from _create_team_table()"""
        n_disc = len(self.disc_names)
        styles = getSampleStyleSheet()
        styleN = styles["Normal"]
        styleN.wordWrap = "CJK"
        styleN.alignment = 1
        disc_cells = [Paragraph(d, styleN) for d in self.disc_names]
        data = [["", f"{wertung_name}-Wertung", "", ""] + disc_cells + ["Punkte"]]
        for t in sorted(teams_cur_wertung, key=lambda x: x.total_rank):
            data.extend(_team_to_row(t, self.disc_names))

        style_list = [
            ("GRID", (0, 1), (-1, -1), 0.5, colors.grey),
            ("ALIGN", (0, 1), (0, -1), "CENTER"),
            ("VALIGN", (0, 1), (0, -1), "MIDDLE"),
            ("ALIGN", (4, 1), (-1, -1), "CENTER"),
            ("VALIGN", (4, 1), (-1, -1), "MIDDLE"),
        ]
        col_points = n_disc + 4
        for ind_team in range(len(teams_cur_wertung)):
            style_list.append(
                ("SPAN", (0, 5 * ind_team + 1), (0, 5 * ind_team + 1 + 4))
            )
            for d in range(len(self.disc_names)):
                style_list.append(
                    ("SPAN", (4 + d, 5 * ind_team + 1), (4 + d, 5 * ind_team + 1 + 1))
                )  # (startcol, startrow) (endcol, endrow)
                style_list.append(
                    (
                        "SPAN",
                        (4 + d, 5 * ind_team + 1 + 2),
                        (4 + d, 5 * ind_team + 1 + 4),
                    )
                )  # (startcol, startrow) (endcol, endrow)
            style_list.append(
                (
                    "SPAN",
                    (col_points, 5 * ind_team + 1),
                    (col_points, 5 * ind_team + 1 + 4),
                )
            )

        t = Table(
            data,
            colWidths=[1 * cm, 4 * cm, 5 * cm, 1.3 * cm]
            + [2.5 * cm] * n_disc
            + [2 * cm],
            style=[*style_list],
        )
        return t


def _team_to_row(t: teams.Team, disc: List[str]) -> List[List[str]]:
    out = []
    results = []
    for d in disc:
        res = t.get_result_from_name(d)
        if res:
            found_disc = t.get_discipline_from_name(d)
            results.append(io_base.results_to_string(res, found_disc.smaller_is_better))
        else:
            results.append("")
    points: list = [
        "" if t.get_points_from_name(d) is None else f"{t.get_points_from_name(d)}"
        for d in disc
    ]
    athletes_names = [""] * 5
    yobs = [""] * 5
    for ind, a in enumerate(t.athletes):
        athletes_names[ind] = a.name
        yobs[ind] = f"{a.yob}"

    out.append(
        [f"{t.total_rank}.", "", athletes_names[0], yobs[0]]
        + results
        + [f"{t.get_total_points()}"]
    )
    out.append(["", t.name, athletes_names[1], yobs[1]] + [""] * (len(results) + 1))
    out.append(["", t.verein, athletes_names[2], yobs[2]] + points + [""])
    out.append(["", "", athletes_names[3], yobs[3]] + [""] * (len(disc) + 1))
    out.append(["", "", athletes_names[4], yobs[4]] + [""] * (len(disc) + 1))
    return out
