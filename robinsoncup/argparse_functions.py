import logging

from robinsoncup import io_base
from robinsoncup import io_in
from robinsoncup import io_reports
from robinsoncup import io_startlist
from robinsoncup import io_out
from robinsoncup import teams


LOGGER = logging.getLogger(__name__)


def to_points(args):
    if args.verbose:
        LOGGER.setLevel(logging.DEBUG)
    LOGGER.info(f"Starting main with arguments: {args}")
    LOGGER.debug(f"Reading input file {args.input}")
    comp, disc, team = io_in.read_input(args.input)
    LOGGER.debug(
        f"Found: {comp}\n {len(disc)} disciplines: {disc}\n "
        f"{len(team)} teams: {team}"
    )

    current_year = comp.date.year
    LOGGER.debug(f"Adding points to teams (current year: {current_year}")
    for d in disc:
        teams.add_points_to_teams(team, d, current_year, points_per=comp.points_per)

    LOGGER.debug(f"Start writing output to {args.output}")
    io_out.write_output(args.output, comp, disc, team, current_year)
    LOGGER.info(f"Output written to {args.output}")


def to_startlist(args):
    if args.verbose:
        LOGGER.setLevel(logging.DEBUG)
    LOGGER.info(f"Starting main with arguments: {args}")
    LOGGER.debug(f"Reading input file {args.input}")
    comp, disc, team = io_in.read_input(args.input)
    LOGGER.debug(
        f"Found: {comp}\n {len(disc)} disciplines: {disc}\n "
        f"{len(team)} teams: {team}"
    )

    LOGGER.debug(f"Start writing output to {args.output}")
    io_startlist.Startlist(
        args.output, comp.name, comp.date, comp.location, [d.name for d in disc], team
    ).create()
    LOGGER.info(f"Output written to {args.output}")

    if not args.no_open:
        LOGGER.debug(f"Opening {args.output}")
        io_base.open_file_in_os(args.output)


def to_results(args):
    if args.verbose:
        LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("Starting producing results")
    LOGGER.debug(f"Reading input file {args.input}")
    comp, disc, team = io_in.read_input(args.input)

    LOGGER.debug(f"Start writing output to {args.output}")
    io_reports.Results(
        out_file=args.output,
        comp_name=comp.name,
        date=comp.date,
        location=comp.location,
        disc_names=[d.name for d in disc],
        team=team,
    ).create()
    LOGGER.info(f"Output written to {args.output}")
    if not args.no_open:
        io_base.open_file_in_os(args.output)

    if args.skip_certificates:
        LOGGER.info("Skip creating certificates")
    else:
        LOGGER.debug(f"Start writing certificates to {args.certificates_output}")
        io_reports.Certificates(
            out_file=args.certificates_output,
            comp_name=comp.name,
            date=comp.date,
            location=comp.location,
            team=team,
            text_only=args.text_only_certificates,
        ).create()
        LOGGER.info(f"Certificates written to {args.certificates_output}")
        if not args.no_open:
            io_base.open_file_in_os(args.certificates_output)
