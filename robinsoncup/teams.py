import logging
from enum import Enum
from typing import List

import dataclasses

import numpy as np

from robinsoncup import disciplines


WERTUNGEN = {
    "U14": (13, 11.00001),  # 2022: 2009, 2011
    "MAXI": (2018 - 2007, 2018 - 2008.24999999),  # 2022: 2011, 2012.249999
    "MEDI": (2018 - 2008.25, 2018 - 2009.4999999),
    "MINI": (2018 - 2009.5, 0),
}


LOGGER = logging.getLogger(__name__)


class PointsPer(Enum):
    Altersklasse = 1  # points are given for MEDI, MAXI.. separately
    Alle = 2  # points are given over all MEDI/MAXI/MINI


@dataclasses.dataclass(frozen=True)
class Athlete:
    name: str
    yob: int


class Team:
    def __init__(self, name: str, verein: str, startnumber: int = None):
        self.name = name
        self.verein = verein
        self.startnumber = startnumber
        self.athletes = []
        self._results = dict()
        self._points = dict()
        self.total_rank = None

    def __repr__(self):
        return f"Team({self.name}, {self.verein})"

    def __eq__(self, other):
        if (
            self.name != other.name
            or self.verein != other.verein
            or self.athletes != other.athletes
            or self.startnumber != other.startnumber
        ):
            return False
        return True

    def get_startnumber(self) -> str:
        if self.startnumber is None:
            return ""
        return str(self.startnumber)

    def add_athlete(self, athlete: Athlete):
        self.athletes.append(athlete)

    def mean_yob(self):
        if self.athletes:
            return np.mean([a.yob for a in self.athletes])

    def add_result(self, disc: disciplines.Discipline, value):
        self._results[disc] = value

    def get_result(self, disc: disciplines.Discipline):
        if disc in self._results:
            return self._results[disc]
        else:
            return None

    def get_result_from_name(self, disc_name: str):
        """Returns result if disc_name is found in results; otherwise None."""
        for d in self._results:
            if d.name == disc_name:
                return self.get_result(d)
        else:
            return None

    def get_disciplines_of_results(self):
        return list(self._results.keys())

    def get_discipline_from_name(self, disc_name: str):
        for d in self._results:
            if d.name == disc_name:
                return d
        else:
            return None

    def add_points(self, disc: disciplines.Discipline, points):
        self._points[disc] = points

    def get_points(self, disc: disciplines.Discipline):
        if disc in self._points:
            return self._points[disc]
        else:
            return None

    def get_points_from_name(self, disc_name: str):
        for d in self._points:
            if d.name == disc_name:
                return self.get_points(d)
        else:
            return None

    def get_total_points(self) -> int:
        """Returns sum of all points. 0 in case no points are there, which
        could be because
        * add_points_to_teams() was never called.
        """
        all_points = [d for d in self._points.values() if d is not None]
        return np.sum(all_points)

    def get_wertung(self, current_year: int):
        """Get ~Maxi, Mini, Medi"""
        if not self.athletes:
            raise RuntimeError("Cannot get wertung if no athletes defined")
        mean_yob = np.mean([a.yob for a in self.athletes])
        return yob_to_wertung(mean_yob, current_year)


def add_points_to_teams(
    teams: List[Team],
    discipline: disciplines.Discipline,
    current_year: int,
    points_per: PointsPer = PointsPer.Altersklasse,
):
    """After results are added to teams, this func adds points to teams"""
    all_existing_wertungen = {t.get_wertung(current_year) for t in teams}
    if points_per == PointsPer.Alle:
        all_existing_wertungen = [all_existing_wertungen]
    else:
        all_existing_wertungen = [{w} for w in all_existing_wertungen]
    for wertung in all_existing_wertungen:
        teams_current_wertung = [
            t for t in teams if t.get_wertung(current_year) in wertung
        ]
        results = [t.get_result(discipline) for t in teams_current_wertung]
        points = _results_to_points(results, discipline.smaller_is_better)
        for p, t in zip(points, teams_current_wertung):
            LOGGER.debug(f"Adding {p} points to {t} ({discipline})")
            t.add_points(discipline, p)


def _results_to_points(res: list, smaller_better: bool) -> List:
    """Best result: 1 point, worst: len points, no result: max

    Usually called from add_points_to_teams() or teams_to_rank()

    Examples
    --------
    >>> _results_to_points([60, 65, 62, 68], True)
    [1, 3, 2, 4]
    >>> _results_to_points([60, 65, 62, 68], False)
    [4, 2, 3, 1]
    >>> _results_to_points([60, None, 65, None, 62, 60, 68], True)
    [1, 6, 4, 6, 3, 1, 5]
    >>> _results_to_points([60, None, 65, None, 62, 60, 65, 68, 65], True)
    [1, 8, 4, 8, 3, 1, 4, 7, 4]
    >>> _results_to_points([60, None, 65, None, 62, 60, 65, 68, 65], False)
    [6, 8, 2, 8, 5, 6, 2, 1, 2]
    >>> _results_to_points([None, None, None], True)
    [1, 1, 1]
    >>> _results_to_points([None, None, None], False)
    [1, 1, 1]
    >>> _results_to_points([2, 2, 2, 2], True)
    [1, 1, 1, 1]
    >>> _results_to_points([2, 2, 2, 2], False)
    [1, 1, 1, 1]

    """
    vals_wo_None = [r for r in res if r is not None]
    points_for_none = 1 + len(vals_wo_None)
    sorted_results = sorted(vals_wo_None, reverse=not smaller_better)
    return [
        1 + sorted_results.index(x) if x is not None else points_for_none for x in res
    ]


def yob_to_wertung(yob, cur_year) -> str:
    for w, limits in WERTUNGEN.items():
        if (cur_year - limits[0]) <= yob <= (cur_year - limits[1]):
            return w
    else:
        raise ValueError("YOB is too old (not allowed)")


def teams_to_rank(teams: List[Team], current_year: int) -> List[int]:
    all_existing_wertungen = {t.get_wertung(current_year) for t in teams}
    team_to_rank = [None] * len(teams)
    for wertung in all_existing_wertungen:
        team_current_wertung = [
            t for t in teams if t.get_wertung(current_year) == wertung
        ]
        ranks_current_teams = _results_to_points(
            [t.get_total_points() for t in team_current_wertung], smaller_better=True
        )
        for ind, t in enumerate(team_current_wertung):
            team_to_rank[teams.index(t)] = ranks_current_teams[ind]

    assert len(team_to_rank) == len(teams)
    return team_to_rank


def add_total_ranks_to_teams(teams: List[Team], current_year: int):
    team_to_rank = teams_to_rank(teams, current_year=current_year)
    for ind, total_rank in enumerate(team_to_rank):
        teams[ind].total_rank = total_rank
