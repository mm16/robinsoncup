import dataclasses
import datetime

from robinsoncup import teams


@dataclasses.dataclass(frozen=True)
class Competition:
    name: str
    date: datetime.datetime
    location: str
    points_per: teams.PointsPer
