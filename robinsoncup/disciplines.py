import dataclasses


@dataclasses.dataclass(frozen=True)
class Discipline:
    name: str
    smaller_is_better: bool
