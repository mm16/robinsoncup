import pytest
import quantities as pq
from typing import List

from robinsoncup import disciplines
from robinsoncup import teams


@pytest.fixture
def disciplines_list():
    d1 = disciplines.Discipline("Sprint", True)
    d2 = disciplines.Discipline("Vortex-Weitwurf", False)
    d3 = disciplines.Discipline("Biathlon-Staffel", True)
    d4 = disciplines.Discipline("1kg Medball", False)
    d5 = disciplines.Discipline("Beidbeniger 3erHop", False)
    return [d1, d2, d3, d4, d5]


def get_empty_teams():
    t = []
    # minis
    t.append(teams.Team("Die Purgstall Flitzers", "UVB Purgstall", 99))
    t[-1].add_athlete(teams.Athlete("Olivia Perera", 2010))
    t[-1].add_athlete(teams.Athlete("Laurent Wurzer", 2010))
    t[-1].add_athlete(teams.Athlete("Gabriel Egger", 2011))
    t[-1].add_athlete(teams.Athlete("Alex Brenneis", 2009))
    t.append(teams.Team("Young Sprinters", "ATSV OMV Auersthal"))
    t[-1].add_athlete(teams.Athlete("Marlen Lexa", 2009))
    t[-1].add_athlete(teams.Athlete("Niclas Bayer", 2010))
    t[-1].add_athlete(teams.Athlete("Emmanuel Egolum", 2010))
    t[-1].add_athlete(teams.Athlete("Benjamin Freiberg", 2009))
    t.append(teams.Team("Die drei Musketiere", "ATSV OMV Auersthal", 2))
    t[-1].add_athlete(teams.Athlete("Hanna Kastner", 2010))
    t[-1].add_athlete(teams.Athlete("Raphael Ralbovsky", 2011))
    t[-1].add_athlete(teams.Athlete("Max Mikolajczyk", 2011))

    # medi
    t.append(teams.Team("Golden Spikes", "ATSV OMV Auersthal"))
    t[3].add_athlete(teams.Athlete("Ben Walter", 2009))
    t[3].add_athlete(teams.Athlete("Katharina Wiltzsch", 2008))
    t[3].add_athlete(teams.Athlete("Eaden Roka", 2007))
    t[3].add_athlete(teams.Athlete("Neve Roka", 2009))
    t.append(teams.Team("Road Runners", "UVB Purgstall"))
    t[4].add_athlete(teams.Athlete("Tobias Schagler", 2009))
    t[4].add_athlete(teams.Athlete("Franziska Paumann", 2009))
    t[4].add_athlete(teams.Athlete("Joseph Stadler", 2008))
    t[4].add_athlete(teams.Athlete("Elias Artmann", 2009))
    t.append(teams.Team("Die coolen Geparde", "UVB Purgstall"))
    t[5].add_athlete(teams.Athlete("Samuel Überlacker", 2008))
    t[5].add_athlete(teams.Athlete("Julius Bewersdorf", 2009))
    t[5].add_athlete(teams.Athlete("Manuel Latzelsberger", 2008))
    t[5].add_athlete(teams.Athlete("Sebastian Lutz", 2008))
    t.append(teams.Team("HSV OMV", "HSV OMV"))
    t[6].add_athlete(teams.Athlete("Sophia Honemann (HSV)", 2009))
    t[6].add_athlete(teams.Athlete("Vanessa Janis (HSV)", 2009))
    t[6].add_athlete(teams.Athlete("Petra Balogh (OMV)", 2007))
    t.append(teams.Team("Fasties", "ATSV OMV Auersthal"))
    t[7].add_athlete(teams.Athlete("Jonas Sheith", 2008))
    t[7].add_athlete(teams.Athlete("Marlena Seidl", 2010))
    t[7].add_athlete(teams.Athlete("Lisa Lexa", 2008))
    t[7].add_athlete(teams.Athlete("Moritz Holmes", 2009))
    t.append(teams.Team("Red Riverside", "ULC Riverside Mödling"))
    t[8].add_athlete(teams.Athlete("Karem Sheith", 2009))
    t[8].add_athlete(teams.Athlete("Marlena Huber", 2009))
    t[8].add_athlete(teams.Athlete("Simon Wildfeuer", 2010))
    t[8].add_athlete(teams.Athlete("Benjamin Wildfeuer", 2008))
    t.append(teams.Team("SVS2", "SVS Leichtathletik"))
    t[9].add_athlete(teams.Athlete("Valentin Volek", 2010))
    t[9].add_athlete(teams.Athlete("Fabian Neubauer", 2009))
    t[9].add_athlete(teams.Athlete("Eric Buzu", 2009))
    t.append(teams.Team("Superflitzer", "ULC Hirtenberg"))
    t[10].add_athlete(teams.Athlete("Romy Prager", 2009))
    t[10].add_athlete(teams.Athlete("Julia Gruber", 2010))
    t[10].add_athlete(teams.Athlete("Raphael Erhart", 2009))
    t.append(teams.Team("Die blauen Blitze", "ATSV Ternitz"))
    t[11].add_athlete(teams.Athlete("Miriam Mainhard", 2009))
    t[11].add_athlete(teams.Athlete("Jasmin Ungersböck", 2009))
    t[11].add_athlete(teams.Athlete("Alexander Jeitier", 2009))
    t[11].add_athlete(teams.Athlete("Tabitha Millner", 2007))
    t[11].add_athlete(teams.Athlete("Bruno Hinum", 2010))
    return t


@pytest.fixture
def team_list(disciplines_list):
    """Data from https://oelvint.athmin.at/api/file.aspx?id=25618 (Robinson
    Cup 2.9.2018 Wien Stadlau, MEDI Wertung"""
    d = disciplines_list
    t = get_empty_teams()
    t = fill_teams(t, d)
    return t


def fill_teams(t: List[teams.Team], d: List[disciplines.Discipline]):
    t[0].add_result(d[0], 32.7 * pq.s)
    t[0].add_result(d[1], 14.53 * pq.m)
    t[0].add_result(d[2], 118.67 * pq.s)
    t[0].add_result(d[3], 37.6 * pq.m)
    t[0].add_result(d[4], 19.9 * pq.m)
    t[1].add_result(d[0], 32.2 * pq.s)
    t[1].add_result(d[1], 13.6 * pq.m)
    t[1].add_result(d[2], 117.71 * pq.s)
    t[1].add_result(d[3], 32.3 * pq.m)
    t[1].add_result(d[4], 17.4 * pq.m)
    t[2].add_result(d[0], 34.4 * pq.s)
    t[2].add_result(d[1], 19.85 * pq.m)
    t[2].add_result(d[2], 155.56 * pq.s)
    t[2].add_result(d[3], 35.5 * pq.m)
    t[2].add_result(d[4], 18.1 * pq.m)
    for dd in d:
        teams.add_points_to_teams(t, dd, 2018)
    # MEDI, sprint
    medi_results = [29.8, 30.1, 30.4, 29.7, 32.9, 32.8, 34.3, 33.2, 38.9] * pq.s
    for tt, res in zip(t[3:], medi_results):
        tt.add_result(d[0], res)
    # vortex
    medi_results = [25.35, 19.98, 21.9, 18.7, 19.45, 19.33, 22.58, 15.4, 16.8] * pq.m
    for tt, res in zip(t[3:], medi_results):
        tt.add_result(d[1], res)
    # biathlon
    medi_results = [
        120.14,
        107.78,
        103.74,
        117.44,
        116.28,
        97.67,
        115.74,
        97.76,
        117.25,
    ] * pq.s
    for tt, res in zip(t[3:], medi_results):
        tt.add_result(d[2], res)
    # medball
    medi_results = [52.5, 48.1, 48.8, 43.9, 46.3, 41.7, 46.3, 47.6, 42] * pq.m
    for tt, res in zip(t[3:], medi_results):
        tt.add_result(d[3], res)
    # beidb 3er hopp
    medi_results = [21.9, 20.4, 20.2, 20.2, 19.8, 18.8, 18.4, 19.3, 20.3] * pq.m
    for tt, res in zip(t[3:], medi_results):
        tt.add_result(d[4], res)
    for dd in d:
        teams.add_points_to_teams(t[3:], dd, 2018)
    return t


def test_teams_get_total_points_zero_if_no_disciplines():
    empty_teams = get_empty_teams()
    assert empty_teams[0].get_total_points() == 0


def test_results_smaller_better_no_exaequo():
    empty_teams = get_empty_teams()
    d = disciplines.Discipline(name="Hindernis", smaller_is_better=True)
    empty_teams[0].add_result(d, 60.27 * pq.s)
    empty_teams[1].add_result(d, 65.32 * pq.s)
    empty_teams[2].add_result(d, 62.69 * pq.s)
    teams.add_points_to_teams(empty_teams, d, 2018)
    assert empty_teams[0].get_points(d) == 1
    assert empty_teams[1].get_points(d) == 3
    assert empty_teams[2].get_points(d) == 2


def test_minis_points(team_list, disciplines_list):
    minis = [t for t in team_list if t.get_wertung(2018) == "MINI"]
    assert minis[0].get_points(disciplines_list[0]) == 2
    assert minis[0].get_total_points() == 8
    assert minis[1].get_total_points() == 11
    assert minis[2].get_total_points() == 11


def test_minis_ranks(team_list):
    current_year = 2018
    minis = [t for t in team_list if t.get_wertung(current_year) == "MINI"]
    got_ranks = teams.teams_to_rank(minis, current_year=current_year)
    assert got_ranks == [1, 2, 2]


def test_medi_points_single_disciplies(team_list, disciplines_list):
    points_per_discipline = [
        [2, 3, 4, 1, 6, 5, 8, 7, 9],
        [1, 4, 3, 7, 5, 6, 2, 9, 8],
        [9, 4, 3, 8, 6, 1, 5, 2, 7],
        [1, 3, 2, 7, 5, 9, 5, 4, 8],
        [1, 2, 4, 4, 6, 8, 9, 7, 3],
    ]
    medis = [t for t in team_list if t.get_wertung(2018) == "MEDI"]
    for ind, d in enumerate(disciplines_list):
        got_points = [t.get_points(d) for t in medis]
        assert got_points == points_per_discipline[ind]


def test_medi_ranks(team_list):
    current_year = 2018
    medis = [t for t in team_list if t.get_wertung(current_year) == "MEDI"]
    exp_ranks = [1, 2, 2, 4, 5, 6, 6, 6, 9]
    got_ranks = teams.teams_to_rank(medis, current_year=current_year)
    assert got_ranks == exp_ranks


def test_points_over_all_wertungen(team_list, disciplines_list):
    for dd in disciplines_list:
        teams.add_points_to_teams(
            team_list, dd, current_year=2018, points_per=teams.PointsPer.Alle
        )
    exp_points_per_disc = [
        [6, 5, 11, 2, 3, 4, 1, 8, 7, 10, 9, 12],
        [11, 12, 5, 1, 4, 3, 8, 6, 7, 2, 10, 9],
        [10, 9, 12, 11, 4, 3, 8, 6, 1, 5, 2, 7],
    ]
    for ind, exp_points in enumerate(exp_points_per_disc):
        got_points = [t.get_points(disciplines_list[ind]) for t in team_list]
        assert got_points == exp_points
