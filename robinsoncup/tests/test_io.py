import csv
import datetime
import io
import os.path
from typing import List

import pytest

from robinsoncup import competition
from robinsoncup import disciplines
from robinsoncup import io_in
from robinsoncup import io_out
from robinsoncup import teams

from robinsoncup import tests


COMPETITION = competition.Competition(
    "Robinson-Cup",
    datetime.datetime(2018, 9, 2),
    "Wien-Stadlau",
    points_per=teams.PointsPer.Altersklasse,
)


DISCIPLINES = [
    disciplines.Discipline("Sprint", True),
    disciplines.Discipline("Vortex-Weitwurf", False),
    disciplines.Discipline("Biathlon-Staffel", True),
    disciplines.Discipline("1kg Medizinballstoß", False),
    disciplines.Discipline("Beidbeiniger 3er-Hopp", False),
]


DIRNAME = os.path.dirname(__file__)


def test_read_example_input():
    t = tests.test_results.get_empty_teams()
    mini_teams = t[:3]
    comp, disc, team = io_in.read_input(
        os.path.join(DIRNAME, "data/example_only_mini.csv")
    )
    assert comp == COMPETITION
    assert disc == DISCIPLINES

    assert team == mini_teams

    exp_startnumbers = [99, None, 2]
    for t, exp_startnumber in zip(team, exp_startnumbers):
        t.startnumber == exp_startnumber
        for d in disc:
            assert t.get_result(d)


def test_read_input_check_points_ranks():
    comp, disc, team = io_in.read_input(
        os.path.join(DIRNAME, "data/example_with_ranks.csv")
    )
    expected_ranks = [9, 8, 8, 7, 6, 5]
    exp_points = {
        disc[0]: [8, 7, 6, 5, 4, 3],
        disc[1]: [30, 29, 28, 27, 26, 25],
        disc[2]: [15, 14, 13, 12, 11, 10],
        disc[3]: [0, 1, 2, 3, 4, 5],
        disc[4]: [6, 5, 4, 3, 2, 1],
    }
    for ind, t in enumerate(team):
        assert t.total_rank == expected_ranks[ind]
        for d in disc:
            assert t.get_points(d) == exp_points[d][ind]


def test_read_input_wrong_delimiter():
    wrong_first_row = io.StringIO("Robinsoncup;;;;;;")
    with pytest.raises(RuntimeError):
        io_in._read_buffer(wrong_first_row)


@pytest.mark.parametrize(
    "rows",
    (
        [["Name"], ["24.12.1900"], ["Wien"]],
        [["Name"], ["24.12.1900"], ["Wien"], [["foo"]]],
    ),
)
def test_read_comp_wrong_wertung_ueber(rows):
    with pytest.raises(AssertionError):
        io_in._read_competition(rows)


def test_read_comp_wrong_wertung_ueber_second_col():
    rows = [["Name"], ["24.12.1900"], ["Wien"], ["Wertung über", "WRONG"]]
    with pytest.raises(ValueError):
        io_in._read_competition(rows)


@pytest.mark.parametrize("add_points", [True, False])
def test_write_output_is_same_as_input(add_points, tmp_path):
    current_year = 2018
    # assert False, "I think there is a bug - too many athletes as empty (if <5)"
    out_file = tmp_path / "output.csv"
    team = tests.test_results.get_empty_teams()
    team = tests.test_results.fill_teams(team, DISCIPLINES)
    if add_points:
        for disc in DISCIPLINES:
            teams.add_points_to_teams(team, disc, current_year=current_year)
    io_out.write_output(out_file, COMPETITION, DISCIPLINES, team, current_year)
    assert_csvfile_only_filled_emptys(
        os.path.join(DIRNAME, "data/example_only_mini.csv"), out_file
    )


def assert_csvfile_only_filled_emptys(csvfile1, csvfile2):
    """Asserts that 2 files are equal except csvfile2 has cells filled which
    are blank in file 1.
    """
    with open(csvfile1) as file1:
        reader1 = csv.reader(file1, delimiter=",")
        rows1 = [r for r in reader1]
        with open(csvfile2) as file2:
            reader2 = csv.reader(file2, delimiter=",")
            rows2 = [r for r in reader2]
            for row1, row2 in zip(rows1, rows2):
                # this only loops over the smaller list
                for col1, col2 in zip(row1, row2):
                    assert (
                        col1 == ""
                        or col1 == col2
                        or col1 == "0" + col2
                        or float(col1) == float(col2)
                    )
                if len(row1) > len(row2):
                    remainings = [col for col in row1[len(row2) :]]
                    assert all(
                        [c == "" for c in remainings]
                    ), f"Not empty elements found in {remainings}"
            if len(rows1) > len(rows2):
                remainings = [col for col in rows1[len(rows2) :]]
                assert all(
                    [col == "" for col in remainings]
                ), f"Not empty element found in {remainings}"


def _simple_read_csv(csvfile) -> List[List[str]]:
    with open(csvfile) as file:
        reader = csv.reader(file, delimiter=",")
        file_content = [r for r in reader]
    return file_content


@pytest.mark.parametrize(
    "points_per,expected_rows",
    [
        (
            teams.PointsPer.Altersklasse,
            {
                11: {13: "MINI", 14: 1, 15: 8, 17: 2, 19: 2, 21: 2, 23: 1, 25: 1},
                12: {13: "MAXI", 14: 1, 15: 7, 17: 1, 19: 1, 21: 1, 23: 2, 25: 2},
                13: {13: "MINI", 14: 2, 15: 11, 17: 1, 19: 3, 21: 1, 23: 3, 25: 3},
                14: {13: "MAXI", 14: 3, 15: 14, 17: 3, 19: 2, 21: 3, 23: 3, 25: 3},
                15: {13: "MINI", 14: 2, 15: 11, 17: 3, 19: 1, 21: 3, 23: 2, 25: 2},
                16: {13: "MAXI", 14: 2, 15: 9, 17: 2, 19: 3, 21: 2, 23: 1, 25: 1},
            },
        ),
        (
            teams.PointsPer.Alle,
            {
                11: {13: "MINI", 14: 1, 15: 21, 17: 5, 19: 5, 21: 4, 23: 4, 25: 3},
                12: {13: "MAXI", 14: 1, 15: 7, 17: 1, 19: 1, 21: 1, 23: 2, 25: 2},
                13: {13: "MINI", 14: 2, 15: 25, 17: 4, 19: 6, 21: 3, 23: 6, 25: 6},
                14: {13: "MAXI", 14: 3, 15: 17, 17: 3, 19: 2, 21: 5, 23: 3, 25: 4},
                15: {13: "MINI", 14: 3, 15: 26, 17: 6, 19: 4, 21: 6, 23: 5, 25: 5},
                16: {13: "MAXI", 14: 2, 15: 9, 17: 2, 19: 3, 21: 2, 23: 1, 25: 1},
            },
        ),
    ],
)
def test_written_output_has_correct_points_and_ranks(
    points_per, expected_rows, tmp_path
):
    out_file = tmp_path / "out.csv"
    # read example file
    comp, disc, team = io_in.read_input(os.path.join(DIRNAME, "data/example.csv"))

    for d in disc:
        teams.add_points_to_teams(team, d, 2018, points_per=points_per)
    io_out.write_output(out_file, comp, disc, team, 2018)

    file_content = _simple_read_csv(out_file)

    for row_ind, val in expected_rows.items():
        for col_ind, cell_content in val.items():
            try:
                val_from_str = int(file_content[row_ind][col_ind])
            except:
                val_from_str = file_content[row_ind][col_ind]
            assert val_from_str == cell_content


def test_written_output_has_correct_points_and_ranks_with_missing_result(tmp_path):
    out_file = tmp_path / "out.csv"
    # read example file
    comp, disc, team = io_in.read_input(
        os.path.join(DIRNAME, "data/example_only_mini_missing_result.csv")
    )

    for d in disc:
        teams.add_points_to_teams(team, d, 2018)
    io_out.write_output(out_file, comp, disc, team, 2018)

    file_content = _simple_read_csv(out_file)

    # row: col: value
    expected_rows = {
        11: {13: "MINI", 14: 1, 15: 7, 17: 2, 19: 2, 21: 1, 23: 1, 25: 1},
        12: {13: "MINI", 14: 3, 15: 13, 17: 1, 19: 3, 21: 3, 23: 3, 25: 3},
        13: {13: "MINI", 14: 2, 15: 10, 17: 3, 19: 1, 21: 2, 23: 2, 25: 2},
    }

    for row_ind, val in expected_rows.items():
        for col_ind, cell_content in val.items():
            try:
                val_from_str = int(file_content[row_ind][col_ind])
            except:
                val_from_str = file_content[row_ind][col_ind]
            assert val_from_str == cell_content
