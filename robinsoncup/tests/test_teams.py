import pytest

import quantities as pq
from robinsoncup import disciplines
from robinsoncup import teams


@pytest.fixture
def team():
    t = teams.Team("superteam", "ATSV OMV Auersthal")
    a1 = teams.Athlete("Tom M", 2015)
    a2 = teams.Athlete("Tom M", 2015)
    a3 = teams.Athlete("Tom M", 2016)
    a4 = teams.Athlete("Tom M", 2016)
    t.add_athlete(a1)
    t.add_athlete(a2)
    t.add_athlete(a3)
    t.add_athlete(a4)
    return t


def test_team_with_athletes(team):
    assert team.mean_yob() == 2015.5


def test_team_get_result_from_name(team):
    d = disciplines.Discipline("Sprint", True)
    team.add_result(d, 9.79 * pq.s)
    assert team.get_result(d) == 9.79 * pq.s
    assert team.get_result_from_name("Sprint") == 9.79 * pq.s
    assert team.get_discipline_from_name("Sprint").name == "Sprint"
    assert team.get_result_from_name("Not-existing") is None
    assert team.get_discipline_from_name("Not-exist") is None


def test_team_get_points_from_name(team):
    d = disciplines.Discipline("Sprint", True)
    team.add_result(d, 9.79 * pq.s)
    assert team.get_points_from_name("Sprint") is None
    teams.add_points_to_teams([team], d, 2018)
    assert team.get_points_from_name("Sprint") == 1


def test_wertungen():
    cur_yr = 2018
    assert teams.yob_to_wertung(2007, cur_yr) == "MAXI"
    assert teams.yob_to_wertung(2008.24, cur_yr) == "MAXI"
    assert teams.yob_to_wertung(2008.25, cur_yr) == "MEDI"
    assert teams.yob_to_wertung(2009.499, cur_yr) == "MEDI"
    assert teams.yob_to_wertung(2009.50, cur_yr) == "MINI"
    assert teams.yob_to_wertung(2012, cur_yr) == "MINI"


def test_wertungen_too_old():
    with pytest.raises(ValueError):
        teams.yob_to_wertung(2000, 2018)


@pytest.fixture
def two_empty_teams():
    t = [teams.Team("TEAM1", "ATSV OMV Auersthal")]
    t[0].add_athlete(teams.Athlete("Maxi", 2010))
    t[0].add_athlete(teams.Athlete("Tom", 2011))
    t[0].add_athlete(teams.Athlete("Mina", 2009))
    t.append(teams.Team("TEAM2", "SVS", 15))
    t[-1].add_athlete(teams.Athlete("Maxi", 2010))
    t[-1].add_athlete(teams.Athlete("Tom", 2011))
    t[-1].add_athlete(teams.Athlete("Mina", 2009))
    t[-1].add_athlete(teams.Athlete("Susi", 2010))
    t[-1].add_athlete(teams.Athlete("Yang", 2010))
    return t


def test_teams_to_rank_no_results_all_first_place(two_empty_teams):
    exp_ranks = [1, 1]
    teams.add_total_ranks_to_teams(two_empty_teams, 2018)
    assert [t.total_rank for t in two_empty_teams] == exp_ranks
