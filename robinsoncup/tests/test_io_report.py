import datetime
import os.path

import pytest
import quantities as pq

from robinsoncup import io_reports
from robinsoncup import disciplines
from robinsoncup import teams
from robinsoncup import tests


DIRNAME = os.path.dirname(__file__)


@pytest.fixture
def disc():
    return [
        disciplines.Discipline("Sprint", True),
        disciplines.Discipline("Vortex-Weitwurf", False),
        disciplines.Discipline("Biathlon-Staffel", True),
        disciplines.Discipline("1kg Medball", False),
        disciplines.Discipline("Beidbeniger 3erHop", False),
    ]


@pytest.fixture
def team(disc):
    team = tests.test_results.get_empty_teams()
    team = tests.test_results.fill_teams(team, disc)
    teams.add_total_ranks_to_teams(team, current_year=2018)
    return team


def test_create_pdf_report(tmp_path, team, disc):
    filename = tmp_path / "out.pdf"
    filename_to_keep = os.path.join(DIRNAME, "data/robinsoncup_2018_report.pdf")
    disc_names = [d.name for d in disc]

    io_reports.Results(
        str(filename),
        "Robinsoncup",
        datetime.datetime(2018, 8, 25),
        "Wien-Stadlau",
        disc_names=disc_names,
        team=team,
    ).create()
    io_reports.Results(
        filename_to_keep,
        "Robinsoncup",
        datetime.datetime(2018, 8, 25),
        "Wien-Stadlau",
        disc_names=disc_names,
        team=team,
    ).create()


def test_create_results_with_missing_disciplines():
    t = [teams.Team("TEAM1", "ATSV OMV Auersthal")]
    t[0].add_athlete(teams.Athlete("Maxi", 2010))
    t[0].add_athlete(teams.Athlete("Tom", 2011))
    t[0].add_athlete(teams.Athlete("Mina", 2009))
    t.append(teams.Team("TEAM2", "SVS", 15))
    t[-1].add_athlete(teams.Athlete("Maxi", 2010))
    t[-1].add_athlete(teams.Athlete("Tom", 2011))
    t[-1].add_athlete(teams.Athlete("Mina", 2009))
    t[-1].add_athlete(teams.Athlete("Susi", 2010))
    t[-1].add_athlete(teams.Athlete("Yang", 2010))
    d = [
        disciplines.Discipline("Sprint", True),
        disciplines.Discipline("Vortex", False),
    ]
    t[0].add_result(d[0], 9.79 * pq.s)


def test_team_to_row():
    disc = [
        disciplines.Discipline("Run", True),
        disciplines.Discipline("Jump", False),
        disciplines.Discipline("Throw", False),
    ]
    t = teams.Team("Teamname", "Clubname")
    t.add_athlete(teams.Athlete("A", 2014))
    t.add_result(disc[0], 9.79 * pq.s)

    teams.add_points_to_teams([t], disc[0], current_year=2022)
    ranks = teams.teams_to_rank([t], current_year=2022)
    for r, t in zip(ranks, [t]):
        t.total_rank = r
    exp_out = [
        ["1.", "", "A", "2014", "9.79", "", "", "1"],
        ["", "Teamname", "", "", "", "", "", ""],
        ["", "Clubname", "", "", "1", "", "", ""],
        ["", "", "", "", "", "", "", ""],
        ["", "", "", "", "", "", "", ""],
    ]
    assert io_reports._team_to_row(t, [d.name for d in disc]) == exp_out

    t.add_athlete(teams.Athlete("B", 2013))
    t.add_athlete(teams.Athlete("C", 2015))
    t.add_athlete(teams.Athlete("D", 2014))
    t.add_athlete(teams.Athlete("E", 2014))
    exp_out = [
        ["1.", "", "A", "2014", "9.79", "", "", "1"],
        ["", "Teamname", "B", "2013", "", "", "", ""],
        ["", "Clubname", "C", "2015", "1", "", "", ""],
        ["", "", "D", "2014", "", "", "", ""],
        ["", "", "E", "2014", "", "", "", ""],
    ]
    assert io_reports._team_to_row(t, [d.name for d in disc]) == exp_out


def test_create_certificates(tmp_path, team):
    filename = tmp_path / "certificates.pdf"
    filename_to_keep = os.path.join(DIRNAME, "data/robinsoncup_2018_certificates.pdf")
    io_reports.Certificates(
        str(filename),
        "Robinsoncup",
        datetime.datetime(2018, 8, 25),
        "Wien-Stadlau",
        team=team,
    ).create()
    io_reports.Certificates(
        filename_to_keep,
        "Robinsoncup",
        datetime.datetime(2018, 8, 25),
        "Wien-Stadlau",
        team=team,
    ).create()
