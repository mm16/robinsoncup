"""This module is basically one test with a full (but small) competition. It
especially tests to create all different outputs at different stages. Idea: See
if also a "non-perfect" and non-finished competition can create startlists,
results and certificates."""
import datetime
import pytest

import quantities as pq

from robinsoncup import competition
from robinsoncup import disciplines
from robinsoncup import io_base
from robinsoncup import io_out
from robinsoncup import io_reports
from robinsoncup import io_startlist
from robinsoncup import teams

CURRENT_YEAR = 2018
OPEN_FILES = False


@pytest.fixture
def compet():
    return competition.Competition(
        "Wettkampf",
        datetime.datetime(2018, 12, 24),
        "Zauberberg",
        teams.PointsPer.Altersklasse,
    )


@pytest.fixture
def team():
    t1 = teams.Team("TEAM1", "ATSV OMV Auersthal")
    t1.add_athlete(teams.Athlete("Maxi", 2010))
    t1.add_athlete(teams.Athlete("Tom", 2011))
    t1.add_athlete(teams.Athlete("Mina", 2009))
    t2 = teams.Team("TEAM2", "SVS", 15)
    t2.add_athlete(teams.Athlete("Maxi", 2010))
    t2.add_athlete(teams.Athlete("Tom", 2011))
    t2.add_athlete(teams.Athlete("Mina", 2009))
    t2.add_athlete(teams.Athlete("Susi", 2010))
    t2.add_athlete(teams.Athlete("Yang", 2010))
    return [t1, t2]


@pytest.fixture
def disc():
    d1 = disciplines.Discipline("Sprint", True)
    d2 = disciplines.Discipline("Vortex-Weitwurf", False)
    return [d1, d2]


def test_small_competition(tmp_path, compet, team, disc):
    # 1: 2 team, 2 disc
    teams.add_total_ranks_to_teams(team, current_year=2018)
    _create_all_outputs(tmp_path, compet, disc, team)

    # 3: add 1 result of 1 disc
    team[0].add_result(disc[1], 65.44 * pq.m)
    teams.add_points_to_teams(
        teams=team,
        discipline=disc[1],
        current_year=CURRENT_YEAR,
        points_per=compet.points_per,
    )
    teams.add_total_ranks_to_teams(team, CURRENT_YEAR)
    _create_all_outputs(tmp_path, compet, disc, team)

    # add all results of same disc
    team[1].add_result(disc[1], 108.50 * pq.m)
    teams.add_points_to_teams(
        teams=team,
        discipline=disc[1],
        current_year=CURRENT_YEAR,
        points_per=compet.points_per,
    )
    teams.add_total_ranks_to_teams(team, CURRENT_YEAR)
    _create_all_outputs(tmp_path, compet, disc, team)

    # add 1 results of 2. disc
    team[0].add_result(disc[0], 25.99 * pq.s)
    teams.add_points_to_teams(
        teams=team,
        discipline=disc[0],
        current_year=CURRENT_YEAR,
        points_per=compet.points_per,
    )
    teams.add_total_ranks_to_teams(team, CURRENT_YEAR)
    _create_all_outputs(tmp_path, compet, disc, team)

    # add 2. results of 2. disc
    team[1].add_result(disc[0], 28.99 * pq.s)
    teams.add_points_to_teams(
        teams=team,
        discipline=disc[0],
        current_year=CURRENT_YEAR,
        points_per=compet.points_per,
    )
    teams.add_total_ranks_to_teams(team, CURRENT_YEAR)
    _create_all_outputs(tmp_path, compet, disc, team)

    # 4: add another team
    team.append(teams.Team("Nachmelde-Team", "Pottenstein"))
    team[2].add_athlete(teams.Athlete("Latzi", 2010))
    team[2].add_athlete(teams.Athlete("Latzi2", 2010))
    team[2].add_athlete(teams.Athlete("Latzi3", 2010))
    team[2].add_athlete(teams.Athlete("Latzi4", 2010))
    for d in disc:
        teams.add_points_to_teams(
            teams=team,
            discipline=d,
            current_year=CURRENT_YEAR,
            points_per=compet.points_per,
        )
    teams.add_total_ranks_to_teams(team, CURRENT_YEAR)
    _create_all_outputs(tmp_path, compet, disc, team)

    # add both results of third team
    team[2].add_result(disc[0], 5.54 * pq.s)
    team[2].add_result(disc[1], 3.54 * pq.m)
    for d in disc:
        teams.add_points_to_teams(
            teams=team,
            discipline=d,
            current_year=CURRENT_YEAR,
            points_per=compet.points_per,
        )
    teams.add_total_ranks_to_teams(team, CURRENT_YEAR)
    _create_all_outputs(tmp_path, compet, disc, team)


def _create_all_outputs(tmp_path, comp, disciplines_list, team_list):
    file1 = tmp_path / "startlist.pdf"
    io_startlist.Startlist(
        out_file=str(file1),
        comp_name=comp.name,
        date=comp.date,
        location=comp.location,
        disc_names=[d.name for d in disciplines_list],
        team=team_list,
    ).create()
    if OPEN_FILES:
        io_base.open_file_in_os(file1)

    file2 = tmp_path / "points.csv"
    io_out.write_output(
        str(file2),
        comp=comp,
        disc=disciplines_list,
        teams_list=team_list,
        current_year=CURRENT_YEAR,
    )
    if OPEN_FILES:
        io_base.open_file_in_os(file2)

    file3 = tmp_path / "results.pdf"
    io_reports.Results(
        out_file=str(file3),
        comp_name=comp.name,
        date=comp.date,
        location=comp.location,
        disc_names=[d.name for d in disciplines_list],
        team=team_list,
    ).create()
    if OPEN_FILES:
        io_base.open_file_in_os(file3)

    file4 = tmp_path / "certificates.pdf"
    io_reports.Certificates(
        out_file=str(file4),
        comp_name=comp.name,
        date=comp.date,
        location=comp.location,
        team=team_list,
    ).create()
    if OPEN_FILES:
        io_base.open_file_in_os(file4)
