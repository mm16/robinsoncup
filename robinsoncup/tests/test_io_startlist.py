import datetime
import os

import pytest

from robinsoncup import disciplines
from robinsoncup import io_in
from robinsoncup import io_startlist
from robinsoncup import teams

from robinsoncup import tests


DIRNAME = os.path.dirname(__file__)


@pytest.fixture
def mock_disc():
    d1 = disciplines.Discipline("Sprint", True)
    d2 = disciplines.Discipline("Vortex-Weitwurf", False)
    d3 = disciplines.Discipline("Biathlon-Staffel", True)
    d4 = disciplines.Discipline("1kg Medball", False)
    d5 = disciplines.Discipline("Beidbeniger 3erHop", False)
    return [d1, d2, d3, d4, d5]


def test_create_startlist(mock_disc, tmp_path):
    team = tests.test_results.get_empty_teams()

    out_file = tmp_path / "startlist.pdf"
    filename_to_keep = os.path.join(DIRNAME, "data/robinsoncup_2018_startlist.pdf")

    io_startlist.Startlist(
        out_file=str(out_file),
        comp_name="Robinsoncup",
        date=datetime.datetime(2018, 12, 24),
        location="Wien-Stadlau",
        disc_names=[d.name for d in mock_disc],
        team=team,
    ).create()
    io_startlist.Startlist(
        out_file=filename_to_keep,
        comp_name="Robinsoncup",
        date=datetime.datetime(2018, 12, 24),
        location="Wien-Stadlau",
        disc_names=[d.name for d in mock_disc],
        team=team,
    ).create()


def test_startlist_also_works_with_only_disc(tmp_path):
    filename = os.path.join(DIRNAME, "data/example.csv")
    with open(filename, encoding="utf-8") as f:
        full_content = f.readlines()
    for take_up_to_line in range(5, len(full_content)):
        comp, disc, team = io_in._read_buffer(full_content[:take_up_to_line])
        io_startlist.Startlist(
            out_file=str(tmp_path / "startlist.pdf"),
            comp_name=comp.name,
            date=comp.date,
            location=comp.location,
            disc_names=[d.name for d in disc],
            team=team,
        ).create()


def test_team_to_startlist_row():
    t = teams.Team("Team", "Club")
    t.add_athlete(teams.Athlete("A", 1980))
    t.add_athlete(teams.Athlete("B", 1981))
    t.add_athlete(teams.Athlete("C", 1982))
    t.add_athlete(teams.Athlete("D", 1983))

    expected_row = [
        ["", "A", "1980", "", "", ""],
        ["Team", "B", "1981", "", "", ""],
        ["Club", "C", "1982", "", "", ""],
        ["", "D", "1983", "", "", ""],
        ["", "", "", "", "", ""],
    ]
    io_startlist._team_to_startlist_row(t) == expected_row
