import csv
from typing import List

from robinsoncup import competition
from robinsoncup import disciplines
from robinsoncup import io_base
from robinsoncup import teams


def write_output(
    file: str,
    comp: competition.Competition,
    disc: List[disciplines.Discipline],
    teams_list: List[teams.Team],
    current_year: int,
):
    with open(file, "w", encoding="utf-8", newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=",")
        _write_competition(writer, comp)
        _write_bewerbe(writer, disc)
        _write_teams(writer, teams_list, disc, current_year=current_year)


def _write_competition(csv_writer: csv.writer, comp):
    csv_writer.writerow([comp.name])
    csv_writer.writerow([comp.date.strftime("%d.%m.%Y")])
    csv_writer.writerow([comp.location])
    csv_writer.writerow(["Wertung über", comp.points_per.name, "(Altersklasse|Alle)"])


def _write_bewerbe(csv_writer, disc):
    csv_writer.writerow(
        ["Bewerbe", "1: Kleiner=Besser (sek); 0: Größer=Besser (meter)"]
    )
    for d in disc:
        csv_writer.writerow([d.name, 1 if d.smaller_is_better else 0])


def _write_teams(csv_writer, team_list, disc, current_year: int):
    csv_writer.writerow(
        [
            "Teams",
            "Verein",
            "Startnummer",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Name",
            "YOB",
            "Altersklasse",
            "Gesamtrang",
            "Gesamtpunkte",
            "Leistung1",
            "Punkte1",
            "Leistung...",
            "Punkte...",
        ]
    )
    total_team_ranks = teams.teams_to_rank(team_list, current_year=current_year)
    altersklasse = [t.get_wertung(current_year) for t in team_list]
    for ind, t in enumerate(team_list):
        athletes = [x for a in t.athletes for x in [a.name, a.yob]]
        athletes = athletes + [""] * (10 - len(athletes))
        results = [
            res
            for d in disc
            for res in [
                ""
                if t.get_result(d) is None
                else io_base.results_to_string(t.get_result(d), d.smaller_is_better),
                t.get_points(d),
            ]
        ]
        csv_writer.writerow(
            [t.name, t.verein, t.get_startnumber()]
            + athletes
            + [altersklasse[ind]]
            + [total_team_ranks[ind]]
            + [t.get_total_points()]
            + results
        )
